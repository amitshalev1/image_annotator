nvidia-smi
if [ $? -eq 0 ]; then
    gpu_or_cpu='gpu'
else
    gpu_or_cpu='cpu'
fi
cd ..
full_path="$PWD"

cd docker_setup/$gpu_or_cpu/
docker build -t annotation_system .


if [ "$gpu_or_cpu" = "gpu" ]; then 
    nvidia-docker run -it -v $full_path:/maagad_annotation_2018/ -p 5012:5002 annotation_system bash prepare_docker.sh
else
    docker run -it -v $full_path:/maagad_annotation_2018/ -p 5012:5002 annotation_system bash prepare_docker.sh
fi