cd ../..
if [ ! -d "$DIRECTORY" ]; then

    mkdir data
    cd data/
    echo '[{"name": "hazera_leaf_cu_mask", "path": "/T/", "user": "dror", "is_alive": 1}]' >> datasets.json
    echo '[{"name": "hazera_leaf_cu_mask", "img_source": "/T/", "possible_values": "1", "trait": "1", "users": "daniel,lena,dror", "img_target": "300", "type": "segmentation", "crop": "cucumber", "object": "leaf", "dev": "seedling", "treatment": "none", "anno_order": "ordered", "allow_repeat": "No", "user_name": "daniel", "task_id": 8}]' >> tasks.json

    echo '[{"user_id": 0, "password": "3558dd008650017d9c6f23ec9da644a85eeb3664", "name": "dror", "is_admin": 1}, {"user_id": 0, "password": "7288edd0fc3ffcbe93a0cf06e3568e28521687bc", "name": "test", "is_admin": 0}, {"user_id": 0, "password": "3d0f3b9ddcacec30c4008c5e030e6c13a478cb4f", "name": "daniel", "is_admin": 1}]
    ' >> users.json
    echo "[]" >> dataset_creation_log.json
    mkdir results
    cd ../img/ && cp lens-833059_640_bdark.jpg lens-833059_640_dark.jpg && cd ..

fi


python -m flask run -h 0.0.0.0 -p 5002