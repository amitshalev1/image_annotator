from flask import Blueprint, request, jsonify
import requests, json

dextrBP = Blueprint('dextrBP', __name__)


#================================ API =======================================
@dextrBP.route('/magic_points_dextr', methods=['POST'])
def magic_points_dextr():
    j = request.get_json(force=True)
    r = requests.post('http://localhost:50122/', json=json.dumps(j))
    return jsonify(r.json())