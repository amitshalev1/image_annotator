#utf-8; python 3.6; flask app
# Phenomics annotation system 1
#/app.py
# Dror hilman 2018

from riotpy import *
from tags.bounce_spinner_tag import *
from tags.navbar import *
from tags.tasks_tag import *
from tags.task_viewer import *
from tags.simple_scoring import *
from tags.explore_data import *
from tags.segmentation_tag import *
from router import *
from users import *
from tasks import *
from simpleScoring import *
from segmentation import *

    
    

class maintag(RiotTag):
    def HTML(self):
        navbar(title='Annotator 1.0', base_color='black', logo='hazera_log_icon_small_white2.png')()
        RiotRouter(default_tag="tasks_tag")()

app = RiotPyApp(toptitle="Hazera Phenomics annotation 0.2", 
                mount=['maintag'],
                body_style =  'background:#263238;',
                js_includes=["js/jquery-3.2.1.min.js","js/tree.jquery.js", "js/materialize.min.js", 'js/echarts.min.js', 'js/Drift.min.js', 'js/paper-full.min.js'],
                css_includes=["css/jqtree.css","css/drift-basic.min.css", "css/materialicons.css", "css/materialize.min.css", "favicon.ico"],
                blueprints=[usersBP, tasksBP, simplescoringBP, segmentationBP]).app

'''@app.before_first_request
def load_dextr():
    import os, sys
    print('loading dxtr server')    

    cwd = os.getcwd()
    dex_wd = cwd + '/DEXTR_flask/'
    if '\\' in dex_wd: dex_wd = dex_wd.replace('/', '\\')
    cmd = sys.executable + ' ' + dex_wd + 'app.py'

    
    os.chdir(dex_wd)
    os.system(cmd)
    os.chdir(cwd)
  
    print('DEXTR started')'''
