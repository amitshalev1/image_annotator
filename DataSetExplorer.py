# a module to handle datsets and connect them to the interface
import json

from flask import Blueprint, request, jsonify, send_file
import json
from tasks import *
import pandas as pd
import numpy as np
import os
import time
from imageDataset import ImageDataset
from datetime import datetime

datasetexplorerBP = Blueprint('datasetexplorerBP', __name__)
DATASETSFILE = 'data/datasets.json'
QFILE = 'data/dataset_q.json'
LOGFILE = 'data/dataset_creation_log.json'

if not os.path.isfile(DATASETSFILE): 
    with open(DATASETSFILE, 'w') as f: f.write('[]')


#---------- utilities ------------------
def sample_max_from_cluster(df, max_in_clust = 20, to_shuffle=False):
    if to_shuffle:
        indxs = [df[df['cluster'] == clust].sample(frac=1.0).index[0:max_in_clust].values
                for clust in df['cluster'].unique() 
                if len(df[df['cluster'] == clust]) > 0]
    else:
        indxs = [df[df['cluster'] == clust].index[0:max_in_clust].values
                for clust in df['cluster'].unique() 
                if len(df[df['cluster'] == clust]) > 0]
    
    indxs = [item for items in indxs for item in items]
    return df.loc[indxs] 

def getImageDatasets():
    if not os.path.isfile(DATASETSFILE): 
        with open(DATASETSFILE, 'w') as f: f.write('[]')
        return []
    with open(DATASETSFILE) as f:
        return json.load(f)

def addImageDataSet(name, path, user, **kwargs):
    datasets = getImageDatasets()
    if len( [d for d in datasets if d['name'] == name and d['is_alive'] == 1] ) > 0:
        return 'dataset already exist!'
    dataset = {'name' : name, 'path' : path, 'user' : user, 'is_alive' : 1}
    datasets.append(dataset)
    with open(DATASETSFILE, 'w') as f: 
        json.dump(datasets, f, ensure_ascii=False)
        newDataSetCreationProcess(dataset)  #!Important: this will spawn a different process 
    return 'saved'

def removeImageDataset(name):
    datasets = getImageDatasets()
    for d in datasets:
        if d['name'] == name:
            d['is_alive'] = 0
            with open(DATASETSFILE, 'w') as f: 
                json.dump(datasets, f, ensure_ascii=False)
            return 'removed'
        else:
            return 'dataset name was not found'


def killImageDatasetCompletely(name):
    datasets = getImageDatasets()
    dataset = [d for d in satasets if not d['name'] == name]
    json.dump(datasets, f, ensure_ascii=False)
    return 'removed'




def get_dataset_creation_Q():
    if os.path.isfile(QFILE): 
        with open(QFILE, 'r') as f: 
            return json.load(f)   
    return []

def add_dataset_to_Q(dataset):
    Q = get_dataset_creation_Q()
    if type(dataset) == list: dataset = dataset[0]
    print('\n\n\n--------------------------')
    print(dataset)
    print(type(dataset))
    print(type(dataset) == list)
    print('--------------------------\n\n\n')
    if len(Q) > 0:
        names = [d['name'] for d in Q]
        if  dataset['name'] in names: 
            return
    Q.append(dataset)
    with open(QFILE, 'w') as f: 
        json.dump(Q, f)  

def pop_dataset_from_Q():    
    Q = get_dataset_creation_Q()
    if len(Q) > 0:
        task = Q.pop(0)
        with open(QFILE, 'w') as f: 
            json.dump(Q, f)
        return task
    else:
        return None

def get_dataset_creation_log():
    if os.path.isfile(LOGFILE):
        with open(LOGFILE, 'r') as f: 
            logs = json.load(f)
        return logs
    return []

def add_task_to_logs(task):
    logs = get_dataset_creation_log()
    logs.append( {'task' : task, 'status' : 'in_progress', 'name' : task['name'], 'created' : str(datetime.now())})
    with open(LOGFILE, 'w') as f:
        json.dump(logs, f)


def update_log_status(datset_name, status):
    logs = get_dataset_creation_log()
    for l in logs:
        if l['name'] == datset_name:
            l['status'] = status
            with open(LOGFILE, 'w') as f:
                json.dump(logs, f)
            return
    
    

def dataSetCreationProcess():
    print('\n\n\n\n -----------------    CREATING DATASET --------------------------------\n\n\n\n')
    logs = get_dataset_creation_log()
    task = pop_dataset_from_Q()

    print('\n\n\n\n -----------------    %s  --------------------------------\n\n\n\n'%(task))
    if task != None:   #check that there is a task in the Q...
        add_task_to_logs(task)
        #now, actually create the dataset!!! ...
        #this may take a while...
        dataset = ImageDataset(project_name=task['name'], imgs_path=task['path'], creator=task['user'], to_hash= True, to_minimize=True, to_bcolz=True)
        update_log_status(task['name'], 'processed')
        dataset._pass_trough_nn()
        update_log_status(task['name'], 'passed trough NN')
        dataset._pca()
        update_log_status(task['name'], 'pca')
        dataset._tsne()
        update_log_status(task['name'], 'done')
        



def newDataSetCreationProcess(dataset):
    add_dataset_to_Q(dataset)
    import multiprocessing
    proc = multiprocessing.Process(target=dataSetCreationProcess)
    proc.start()
    

#================================ API =======================================
@datasetexplorerBP.route('/get_image_datasets')
def get_image_datasets_api():
    return jsonify( getImageDatasets())

@datasetexplorerBP.route('/get_image_datasets_learning_status')
def get_image_datasets_learning_status_api():
    with open(LOGFILE) as f:
         return f.read()

@datasetexplorerBP.route('/add_dataset', methods=['POST'])
def add_dataset_api():
    dataset = request.get_json(force=True)
    return addImageDataSet(**dataset)


@datasetexplorerBP.route('/remove_imagedataset/<dataset_name>')
def remove_imagedataset(dataset_name):
    removeImageDataset(dataset_name)
    return 'removed'

#==================================
@datasetexplorerBP.route('/load_tsne_visualization/<dataset_name>')
def load_tsne_visualization(dataset_name):
    ds = ImageDataset(dataset_name)
    df = ds.pick_images_by_clusters()
    df = sample_max_from_cluster(df)
    return df.to_json(orient='values')

if __name__ == '__main__':
    pass