# 2018 Dror Hilman
#a module to allow feature learning - ver 1.0
from imageDataset import *
import numpy as np
import json

from flask import Blueprint, request, jsonify, send_file
from multiprocessing import Process

dsFeatureLearnerBP = Blueprint('dsFeatureLearnerBP', __name__)

def get_keras_binary_model(n_inputs, layers=[80, 50, 30], dropout=0.3, use_batch_norm=False):
    '''use to generate a fully-connected model on top of pretrained conv network'''
    from keras.models import Model
    from keras.layers import Input, Dense, Dropout, BatchNormalization
    from keras.optimizers import Adam
    print('\n\n --- creating a new model ----- \n\n')
    inp = Input((n_inputs,), name='inp')
    x = inp
    if use_batch_norm:
        x = BatchNormalization()(x)
    for idx, l in enumerate(layers):
        x = Dropout(dropout)(x)
        x = Dense(l, activation='relu', name='dense_%s_%s'%(idx, l))(x)
        if use_batch_norm:
            x = BatchNormalization()(x)
    out = Dense(1, activation='sigmoid', name='out')(x)
    model = Model(inp, out)
    model.compile(Adam(lr=0.0005, clipnorm=1., clipvalue=0.5), 'binary_crossentropy')
    print(model.summary())
    return model

def learning_process(dataset, feature):
    ''' a tand alone process to facilitate learning in a background process'''
    learner = ImageDatasetFeatureLearner(dataset)
    status = learner.get_learning_status(feature)
    if status in ['ok', 'preparing to learn...']:
        learner.learn(feature, epochs=50, to_balance=True, report_ok=False)
        learner.learn(feature, epochs=300, patience=30, to_balance=True)
    return 'learning!'

class ImageDatasetFeatureLearner(ImageDataset):
    '''a module to include all feature learning abilities'''
    def __init__(self, *kw, **kwargs):
        super().__init__(*kw, **kwargs)
        self._pass_trough_nn()

    
    def update_feature_examples(self, feature_name, examples=[]):
        feature_name += '__learned_feature__'
        assert (feature_name in self.dataset.columns), 'feature was not created yet !'
        df = self.dataset
        #df[feature_name] = np.zeros(len(df)).astype('int32')
        if len(examples) > 0:
            examples_idx = df[df['HASH'].isin(examples)].index
            df.loc[examples_idx,feature_name] = 1
            fname = f'cache/{self.name}_dataset_map.parquet'
            self.dataset.to_parquet(fname)

    
    def create_new_feature(self, feature_name, examples=[]):
        feature_name_f = feature_name + '__learned_feature__'
        self.dataset[feature_name_f] = np.zeros(len(self.dataset)).astype('int32')
        self.update_feature_examples(feature_name, examples)

    def get_feature_examples(self, feature_name):
        feature_name += '__learned_feature__'
        examples = self.dataset[self.dataset[feature_name] != 0]['HASH'].values
        return examples

    def get_feature_list(self):
        return [c.split('__learned_feature__')[0] for c in self.dataset.columns if c.endswith('__learned_feature__')]
    
    def delete_feature(self, feature_name):
        feature_name += '__learned_feature__'
        if feature_name in self.dataset.columns:
            del self.dataset[feature_name]
            fname = f'cache/{self.name}_dataset_map.parquet'
            self.dataset.to_parquet(fname)

    def get_training_examples(self, feature_name, to_balance=False, to_shuffle=True):
        from keras.utils import normalize
        feature_name += '__learned_feature__'
        df = self.dataset
        df.index = list(range(len(df)))
        pos_ex = df[df[feature_name] == 1].index
        neg_ex = df[df[feature_name] == 0].index
        if to_balance:
            if   len(neg_ex) > len(pos_ex): neg_ex = neg_ex[0:len(pos_ex)]
            elif len(neg_ex) < len(pos_ex): pos_ex = pos_ex[0:len(neg_ex)]
        X = np.concatenate([self.nn_last_layer.values[pos_ex], self.nn_last_layer.values[neg_ex]], axis=0)
        Y = np.concatenate([np.ones(len(pos_ex)), np.zeros(len(neg_ex))])
        if to_shuffle:
            p = np.random.permutation(len(Y))
            X, Y = X[p], Y[p]
        
        X = normalize(X)  #???
        
        return X, Y

    def get_model(self, feature_name, layers=[100, 50, 50], dropout=0.3):
        model_name = 'model_'+feature_name
        model_fname = f'cache/{self.name}_{model_name}.h5'
        status = 'new'
        if not hasattr(self.__dict__, model_name):
            if os.path.isfile(model_fname):
                from keras.models import load_model
                self.__dict__[model_name] = load_model(model_fname)
                status = 'loaded'
            else:
                n_features = self.nn_last_layer.shape[1]
                self.__dict__[model_name] = get_keras_binary_model(n_features, layers, dropout)
        return self.__dict__[model_name], status

    def learn(self, feature_name, epochs=1000, patience=50, to_balance=True, report_ok=True):
        from keras.callbacks import EarlyStopping, ModelCheckpoint
        self.set_learning_status(feature_name, 'starting')
        
        model_name = 'model_'+feature_name
        model_fname = f'cache/{self.name}_{model_name}.h5'
        x,y = self.get_training_examples(feature_name, to_balance=to_balance)
        model, status = self.get_model(feature_name)
        
        cb = [EarlyStopping(monitor='loss', patience=patience), 
              ModelCheckpoint(model_fname, monitor='loss')]
        
        self.set_learning_status(feature_name, 'learning')
        try:
            model.fit(x,y, epochs=epochs, callbacks=cb)
        except Exception as e:
            self.set_learning_status(feature_name, 'problem during learning! %s'%(str(e)))
        
        if report_ok: self.set_learning_status(feature_name, 'ok')
    
    def predict_by_feature(self, feature_name):
        model, status = self.get_model(feature_name)
        if status == 'new':
            self.learn(feature_name, epochs=100)
        x = self.nn_last_layer.values
        return model.predict(x).ravel()
    
    def fetch_next_annotations_ends(self, feature_name, sample_size=20, true_bound=0.6, false_bound=0.4):
        '''suggest next learning samples, based on the ends of the analysis'''
        self.dataset[feature_name+"_predicted"] = self.predict_by_feature(feature_name)
        df = self.dataset.sort_values(feature_name+"_predicted")
        featlearned = feature_name+"_predicted"
        df = df[df[feature_name + '__learned_feature__'] != 1] #remove known samples
        df = df[['HASH', featlearned]]
        
        predicted_true = df[df[featlearned] > true_bound]['HASH']
        predicted_true_samp = df[df[featlearned] > true_bound].values.tolist()
        predicted_true = predicted_true[-sample_size:].tolist()
        
        predicted_false = df[df[featlearned] < false_bound]['HASH']
        predicted_false_samp = df[df[featlearned] < false_bound].values.tolist()
        predicted_false = predicted_false[:sample_size].tolist()
        
        return {'predicted_true'     : predicted_true,      'predicted_false'     : predicted_false, 
                'predicted_true_samp': predicted_true_samp, 'predicted_false_samp': predicted_false_samp}

    def get_current_positives(self, feature_name, get_max=200):
        df = self.dataset
        if get_max == None: get_max = len(df)
        return df[df[feature_name + '__learned_feature__'] == 1].values.tolist()[0:get_max]

    def set_learning_status(self, feature, status):
        fname = f'cache/learner_{self.name}_{feature}_learn_status.txt'
        with open(fname, 'w') as f:
            f.write(status)
    
    def get_learning_status(self, feature):
        fname = f'cache/learner_{self.name}_{feature}_learn_status.txt'
        if os.path.isfile(fname):
            with open(fname) as f:
                return f.read()
        return 'no learning yet'
    
    def turn_feature_to_a_dataset(self, feature):
        'STILL NOT WORKING!! - NEED TO CREATE THE bcolz files (?!)'
        new_name = feature + "__@__" + self.name
        old_name = self.name

        df = self.dataset
        self._pass_trough_nn()
        self._pca()
        self._tsne()
        
        #filter only positive examples of the feature
        df = df[df[feature + '__learned_feature__'] == 1] 
        df = df[[c for c in df.columns if not c.endswith('__learned_feature__')]] #remove features from the new dataset...
        
        print('filtering the data for positive examples')
        self.nn_last_layer = self.nn_last_layer.loc[df.index]
        self.pca = self.pca.loc[df.index]
        self.tsne = self.tsne.loc[df.index]

        print('caching files...')
        self.tsne.to_parquet(f'cache/tsne_2_last_layer_{new_name}.parquet')
        self.pca.to_parquet(f'cache/pca_None_last_layer_{new_name}.parquet')
        self.nn_last_layer.to_parquet(f'resnet50_last_layer_{new_name}.parquet')
        self.dataset.to_parquet(f'cache/{new_name}_dataset_map.parquet')

        print('caching directories...')
        import os
        hashed_dir = f'cache/project_hashed_images{new_name}'
        small_dir = f'cache/project_small_{new_name}'
        if not os.path.isdir(hashed_dir): os.mkdir(hashed_dir)
        if not os.path.isdir(small_dir): os.mkdir(small_dir)
        
        print('creating symbolic links...')
        for h in tqdm(df['HASH'].values):
            for lib_type, prefix in [('cache/project_hashed_images', ''), ('cache/project_small_', 'small_224_')]:
                src = f'{lib_type}{old_name}/{prefix}{h}.jpg'
                dst = f'{lib_type}{new_name}/{prefix}{h}.jpg'
                if not os.path.islink(dst): os.symlink(src, dst)
        

        #bcolz_dir = f'cache/project_bcolz_{new_name}' # creating bcolz ????
        
        print('saving new project')
        #cache project data
        self.dataset = df
        self.name = new_name
        self._cache_project_data()

        from DataSetExplorer import getImageDatasets, DATASETSFILE
        datasets = getImageDatasets()
        if not new_name in [d['name'] for d in datasets]:
            datasets.append( {'name' : new_name, 'path' : self.imgs_path, 'user' : self.creator, 'is_alive' : 1} )
            with open(DATASETSFILE, 'w') as f:  json.dump(datasets, f, ensure_ascii=False)


        return datasets
            
            
        
        
            

#====================   API ====================
@dsFeatureLearnerBP.route('/add_new_feature_to_dataset', methods=['POST'])
def add_new_feature_to_dataset():
    j = request.get_json(force=True)
    learner = ImageDatasetFeatureLearner(j['dataset'])
    if j['feature_name'] in learner.get_feature_list():
        return 'feature already exists'
    else:
        try:
            learner.create_new_feature(j['feature_name'], j['examples'])
            return 'feature_created'
        except Exception as e:
            return 'problem in creating feature! %s'%(str(e))
        
        
@dsFeatureLearnerBP.route('/get_all_features/<dataset>')
def get_all_features(dataset):
    learner = ImageDatasetFeatureLearner(dataset)
    return jsonify(learner.get_feature_list())

@dsFeatureLearnerBP.route('/get_learning_examples/<dataset>/<feature>')
def get_learning_examples(dataset, feature):
    learner = ImageDatasetFeatureLearner(dataset)
    res = learner.fetch_next_annotations_ends(feature)
    return jsonify(res)


@dsFeatureLearnerBP.route('/get_learning_status/<dataset>/<feature>')
def get_learning_status_api(dataset, feature):
    learner = ImageDatasetFeatureLearner(dataset)
    return learner.get_learning_status(feature)

@dsFeatureLearnerBP.route('/store_and_learn', methods=['POST'])
def store_and_learn_api():
    j = request.get_json(force=True)
    learner = ImageDatasetFeatureLearner(j['dataset'])
    learner.update_feature_examples(j['feature'], j['examples']['predicted_true'])
    status = learner.get_learning_status(j['feature'])
    if status == 'ok':
        print('\n\n\n\n learning...\n\n\n\n')
        learner.set_learning_status(j['feature'], 'preparing to learn...')
        p = Process(target=learning_process, args =(j['dataset'],j['feature'],))
        p.start()
    return 'learning started'

@dsFeatureLearnerBP.route('/get_examples_learned_before', methods=['POST'])
def get_examples_learned_before_api():
    j = request.get_json(force=True)
    '''TODO: check if the feature was already learnt before, if it does, 
    do a prediction and return the predicted true and false'''
    pass
    

@dsFeatureLearnerBP.route('/remove_feature/<dataset>/<feature>')
def remove_feature_api(dataset, feature):
    learner = ImageDatasetFeatureLearner(dataset)
    try:
        learner.delete_feature(feature)
        return "deleted"
    except Exception as e:
        return str(e)


@dsFeatureLearnerBP.route('/get_current_positives/<dataset>/<feature>')
def get_current_positives_api(dataset, feature):
    learner = ImageDatasetFeatureLearner(dataset)
    return jsonify(learner.get_current_positives(feature))
        