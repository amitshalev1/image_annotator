from riotpy import *
#===========================================================
class mdTxtInp(RiotTag):
    def HTML(self):
        with col('{opts.col_class} input-field'):
            input(id='{opts.id_}', type='text',value='{opts.folder_path}')()
            label(for_='{opts.id_}', content='{opts.label}')()


#===========================================================
class mdSelectInput(RiotTag):
    def HTML(self):
        with col('{opts.col_class} input-field'):
                with select(id='{opts.id_}'):
                    option(value='', content="{opts.label}", disabled='', selected='', style='color:grey')()
                    option(each = "{item in opts.items}", value = '{item}', content='{item}')() 
                label("{opts.label}")()
    
       

#===========================================================
class mdBtn(RiotTag):
    def HTML(self):
        with a(css_class='waves-effect waves-light btn {opts.color}', style='padding-left:5px; padding-right:5px;'):
            i(show= '{opts.icon}', css_class='material-icons left', content='{opts.icon}', style='margin-left:0; margin-right:8px;')()
            content('{opts.text}')

            
class mdmultiSelectInput(RiotTag):
    def HTML(self):
        with col('{opts.col_class} input-field'):
                with select(id='{opts.id_}', multiple=''):
                    option(value='', content="{opts.label}", disabled='', selected='', style='color:grey')()
                    option(each = "{item in opts.items}", value = '{item}', content='{item}')() 


                label("{opts.label}")()  

class foldertreemodal(RiotTag): 
    def HTML(self):        
        with div(id="{opts.modal_id}",class_="modal modal-fixed-footer"):
            with div(class_="modal-content"):
                h4(content="{opts._head}")()
                div(id="tree1", data__url="/",style="cursor: pointer")()
            with div(class_="modal-footer"):
                label(content="chosen directory: {folder_path}")()  
                a(href="#",class_="modal-close waves-effect waves-green btn-flat",content="Ok")()
    def JS(self):
        @on("mount")
        def mount():
            def loaded(res):
                va=res 
                jQuery('#tree1').tree({data:va,autoOpen:0})
                jQuery('#tree1').on('tree.click',treesel)
            jQuery.get('folder_tree', loaded, 'json')
            jQuery('.modal').modal()

                
        @make_self
        def treesel(e):
            va=e.node
            res=''
            while va.name!=opts.direc:
                res=va.name+'/'+res
                va=va.parent
            res=opts.direc+res
            me.parent.folder_path=res
            me.folder_path=me.parent.folder_path
            me.parent.update()
            me.update()
            jQuery('#new_task_img_source').trigger('focus')

class teXttag(RiotTag):
    def HTML(self):  
        with div(class_="input-field col s12"):
            with form(class_="col s12"):
                with div(class_="input-field col s6"):
                    textarea(id_="{opts.id_}",class_="materialize-textarea",data__length="120",onchange="{chng}")()
                    label(for_="{opts.id_}",content="{opts.label}")()
    def JS(self):
        @on("mount")
        def mount():
            jQuery('input#input_text, textarea#'+opts.id_).characterCounter()

        @make_self
        def chng(e):
            me.parent.ret=e.target.value


