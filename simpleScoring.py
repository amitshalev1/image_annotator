from flask import Blueprint, request, jsonify, send_file
import json
from tasks import *
import pandas as pd
import os
import time



SCORING_DIR = 'data/results/'
IMAGE_ENDS = ['jpg', 'png', 'gif', 'tif', 'tiff', 'iff']

simplescoringBP = Blueprint('simplescoringBP', __name__)


def get_score_fname(task, user):
    return SCORING_DIR+'phenotype_%s_user_%s.tab'%(task['task_id'], user)

# def get_images_list(task):
#     '''get a list of all the images of this task'''
#     path = task['img_source']
#     if not os.path.isdir(path):
#         return 'path not found'
#     return [path+f for f in os.listdir(path) if f[-3:] in IMAGE_ENDS]
        
def get_images_list(task_id):
    res=get_images_under_directory(task_id.get('img_source'))
    #print(res)
#     res=pd.DataFrame(dummy.get_images_by_experiment_id(task_id.get('task_id'))).image_uri.values.tolist()
    return res
    
#do not cache this function
def get_annotated_images_list(task, user):
    '''get a list of the images that were already annotated'''
    fname = get_score_fname(task, user)
    if not os.path.isfile(fname):
        with open(fname, 'w') as f:
            f.write('\t'.join(['user','date','task','trait','image','score']) + '\n')
        return []
    df = pd.read_table(fname)
    return df['image'].values.tolist()

def images_annotation_lists(task, user):
    annotated = get_annotated_images_list(task, user)
    return {"annotated" : annotated,
            "to_annotate" : [f for f in get_images_list(task) if not f in annotated]}
    

def save_annotation(task, user, image, score):
    try:
        fname = get_score_fname(task, user)
        date = time.strftime("%d/%m/%Y %H:%M:%S")
        with open(fname, 'a') as f:
            line = [str(i) for i in [user, date, task['task_id'], task['trait'], image, score]]
            f.write('\t'.join(line) + '\n')
        return 'OK'
    except Exception as e:
        return str(e)
    

def merge_task_results(trait, task_id):
    initial = 'phenotype_%s_task_%s'%(trait, task_id)
    to_merge = [SCORING_DIR + f for f in os.listdir(SCORING_DIR) if f.startswith(initial)]
    if len(to_merge) > 0:
        merged = pd.concat([pd.read_table(f) for f in to_merge])
    else:
        merged = pd.DataFrame([])

    merged.to_csv('cache/%s.tab'%(initial), sep='\t', index=False)
    return 'cache/%s.tab'%(initial)

#============================  API ==========================
# 
@simplescoringBP.route('/simple_scoring_get_image_list', methods=['POST'])
def simple_scoring_get_image_list():
    j = request.get_json(force=True)
    return jsonify(images_annotation_lists(j['task'], j['user']))


@simplescoringBP.route('/annotate_image', methods=['POST'])
def annotate_image():
    j = request.get_json(force=True)
    return save_annotation(j['task'], j['user'], j['image'], j['score'])


@simplescoringBP.route('/get_task_results_per_user/<trait>/<task_id>/<user>')
def get_task_results_per_user(trait, task_id, user):
    fname = get_score_fname({'trait' : trait, 'task_id' : task_id}, user)
    return send_file(fname, mimetype='text/tab-separated-values', as_attachment=True)

@simplescoringBP.route('/get_all_task_results/<trait>/<task_id>')
def get_all_task_results(trait, task_id):
    fname = merge_task_results(trait, task_id)
    return send_file(fname, mimetype='text/tab-separated-values', as_attachment=True)


@simplescoringBP.route('/load_scale/<trait>/<task_id>/<user>')
def load_scale(trait, task_id, user):   
    fname =  SCORING_DIR+'scale_%s_task_%s_user_%s.json'%(trait, task_id, user)
    try:
        return send_file(fname, mimetype='text/json', cache_timeout=-1)
    except:
        return '[]'


@simplescoringBP.route('/save_scale', methods=['POST'])
def save_scale():   
    j = request.get_json(force=True)
    print(j)
    fname =  SCORING_DIR+'scale_%s_task_%s_user_%s.json'%(j['trait'], j['task_id'], j['user'])
    with open(fname, 'w') as fl: 
        json.dump(j['scale'], fl)
    return "OK"


#add the possibility you refer to th /T drive from linux
@simplescoringBP.route('/T/<path:path>')
def route_image_from_T_drive(path):
    import os
    if os.name == 'nt':   #if on wondows...
        return send_file('T:/'+path)
    else:
        return send_file('/T/'+path+'.JPG')


@simplescoringBP.route('/sample_image/<task_id>/<amount>')
def sample_image(task_id, amount):
    import random
    task = get_task_by_id(task_id)
    imgs = get_images_list(task)
    sample = random.sample(imgs, int(amount))
    return jsonify(sample)
