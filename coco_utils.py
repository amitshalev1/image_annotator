#building the MSCOCO FORMAT ANNOTATIONS...
#http://cocodataset.org/#download
import os
import json
import numpy as np
import shutil 
from tqdm import tqdm 
import cv2 
from urllib.parse import unquote

SCORING_DIR = '../results/'

def read_json(jfile):
    with open(jfile) as f: return json.load(f)

def PolyArea(x,y):
    return 0.5*np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))


def mult_by_factor(arr, factor):
    return [int(a * factor) for a in arr]
    
def reduce_data_set_images_size(dataset_json_file, target_ratio):
    with open(dataset_json_file) as f: j = json.load(f)
    for im in tqdm(j['images'], leave=False):
        im['width']  = int(im['width']  * target_ratio)
        im['height'] = int(im['height'] * target_ratio) 
        
        img = cv2.imread(im['path'])
        img = cv2.resize(img, (im['width'], im['height']))
        cv2.imwrite(im['path'], img)
        
    for a in tqdm(j['annotations'], leave=False):
        a['bbox']             = mult_by_factor(a['bbox'],         target_ratio)
        a['segmentation'][0]  = mult_by_factor(a['segmentation'][0], target_ratio)
        a['segmentation'][1]  = mult_by_factor(a['segmentation'][1], target_ratio)
        a['area'] = PolyArea(a['segmentation'][0], a['segmentation'][1])
    with open(dataset_json_file, 'w') as f: json.dump(j, f)

def get_images(jsons, package_images_target):
    res = []
    for idx, j in enumerate(jsons):
        path = j['img'].split('/')[3:]
        try:
            file_name = path[-1]
            if package_images_target != None:
                if package_images_target.endswith('.zip'): path = path[-1]
                else: path = '/'.join([package_images_target.rstrip('/'), path[-1]])
                
            res.append({
                'file_name' : file_name,
                'path' : path,
                'url' : j['img'],
                'height' : j['img_height'],
                'width' : j['img_width'],
                'date' : j['date'],
                'id' : idx
            })
        except Exception as e:
            print('Problem finding image for zipping', str(e))

    return res

def get_annotations(jsons):
    res = []
    anno_count = 0
    for idx, j in enumerate(jsons):
        w, h = j['img_width'], j['img_height']
        for cont in j['contours']:
            if len(cont) > 0:
                x, y = (np.array(cont) / j['annotation_ratio']).astype(int).T
                #make sure that these coordinates actually creates a polygon ... (x > 2)
                if len(x) > 2:
                    x[x<0] = 0 #fix negative values
                    y[y<0] = 0 #fix negative values
                    x[x>w] = w #fix negative values
                    y[y>h] = h #fix negative values
                    res.append({
                        "id": anno_count,
                        "segmentation": [ [int(c) for coord in list(zip(x,y)) for c in coord] ],
                        "image_id" : j['image_id'],
                        "area" : PolyArea(x, y),
                        "category_id" : 1,
                        "bbox" : [min(x), min(y), max(x) - min(x), max(y) - min(y)],
                        'iscrowd' : 0
                    })
                    anno_count +=1
    return res
        
def get_categories(jsons):
    cat = {}
    for idx, j in enumerate(jsons):
        supercategory = '_unimplemented'
        name = '_unimplemented'
        cat[supercategory] = cat.get(supercategory, {})
        cat[supercategory][name] = cat[supercategory].get(name, idx+1)
    res = [{'supercategory' : s,
            'name' : n,
            'id' : v }
            for s in cat.keys()
            for n, v in cat[s].items()]
    return res
              
    
def get_info(j):
    return {
        'description' : " ".join(['segmentation:',j['user'], j['date']]),
        'date_created' : j['date'],
        'version' : j.get('version', 1.0),
        'contributur' : 'PHENOMICS HAZERA ISRAEL'
    }

def fix_json_fname(url):
    old_path = '/'.join(url.split('/')[3:])
    if os.name == 'nt': old_path = old_path.replace('T/', 'T:/') 
    else:               old_path = old_path.replace('T/', '/T/')
    if '%' in old_path: old_path = unquote(old_path).replace('+', ' ') #fix hebrew and spaces problem
    return old_path

def get_task_lib_by_id(task_id):
    return SCORING_DIR+'annotation_task_%s'%(task_id)
    


def mscoco_exporter(task_lib, json_out=None, package_images_target=None, size_ratio_reduction=None):
    #http://cocodataset.org/#download
    '''converts segmentation data into a mscoco format
       by default it does not package the images with the data, 
       unless *package_images_target* is provided 
       It gets a library with json files, and return one json file with references to the images
       if *json_out* not None: will save the data into this file
       *size_ratio_reduction: if to change the size of the images and annotation by factor *size_ratio_reduction*
       
    '''
    if os.name == 'nt': task_lib = task_lib.lstrip('/').replace('T/', 'T:/')
    lib = task_lib.replace('\\','/').rstrip('/') + '/'
    jsons = [read_json(lib + f)  for f in os.listdir(task_lib)  if f.endswith('.json')]
    jsons = [j for j in jsons if len(j['paths']) > 0 and j.get('is_done', 0)=='1']
    res = {
        'categories' : get_categories(jsons),
        'images' : get_images(jsons, package_images_target),
        'annotations' : get_annotations(jsons),
        'info' : get_info(jsons[0])
    }
    if package_images_target != None:
        
        if not package_images_target.endswith('.zip'):
            if not os.path.isdir(package_images_target): os.mkdir(package_images_target)
            for j in tqdm(res['images']):
                if not os.path.isfile(j['path']):
                    old_path = fix_json_fname(j['url'])
                    shutil.copyfile(old_path, j['path'])
       
        else:  #create a zip file...
            from zipfile import ZipFile
            
            with ZipFile(package_images_target, 'w') as z:
                def default(o):
                    if isinstance(o, np.int64): return int(o)  
                    raise TypeError

                with open('segmentation_results.json', 'w') as f:
                    json.dump(res, f, ensure_ascii=False, allow_nan=False, default=default)
                z.write('segmentation_results.json')

#                 for j in tqdm(res['images']):
#                     old_path = fix_json_fname(j['url'])
#                     if os.path.isfile(old_path):
#                         arcname = old_path.split('/')[-1]
#                         z.write(old_path, arcname)
#                     else:
#                         print(f'Warning!: {old_path} cannot be found')
                
            return package_images_target
            
        
    if json_out != None:
        with open(json_out, 'w') as f:
            json.dump(res, f, ensure_ascii=False, allow_nan=False)
        return 'saved, %s'%(json_out)

        if size_ratio_reduction != None:
            reduce_data_set_images_size(json_out, size_ratio_reduction)
        
    return res


#--------------   Helpers for the UI ---------

def segmentation_task_json_zip(task_id):
    zfile = 'cache/segmentation_task_%s.zip'%(task_id)
    task_lib = get_task_lib_by_id(task_id)
    mscoco_exporter(task_lib, package_images_target=zfile)
    return zfile

def segmentation_task_json_zip_local_store(task_id, store_path='/T/Phenotyping_Lab/Maagad/Annotation results/'):
    import time
    if os.name == 'nt' and store_path.startswith('/'): store_path = store_path[1] + ":" + store_path[2:]
    curtime = time.strftime("%Y_%m_%d_%H_%M")
    zfile = f'{store_path}/{curtime}_segmentation_task_{task_id}.zip'
    if not os.path.isfile(zfile):
        task_lib = get_task_lib_by_id(task_id)
        mscoco_exporter(task_lib, package_images_target=zfile)
    return zfile