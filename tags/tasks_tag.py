from riotpy import *
from riotpy_helper import mdTxtInp,teXttag,mdBtn

class tasks_tag(RiotTag):
    def HTML(self):
        bg = 'background:url(img/lens-833059_640_dark.jpg);background-repeat: no-repeat;background-size: 100%'
        with div(class_='container z-depth-2 black', style = 'width:98%;min-height:90%; {}'.format(bg)):
            with div(class_='card blue-grey darken-4 white-text', style='opacity:0.8'):
                with div(class_='card-content'):
                    a(href='#task/', class_='white-text', content='{address}', style='text-decoration:underline; cursor:pointer')()
                    teXttag(id_="new_task_json",label="asdasd")()
                    mdTxtInp(col_class='s3', id_='new_task_json_add', label='* task name')()
                    mdTxtInp(col_class='s8', id_='new_task_img_source', label='images source (dir/file/query/dataset)',folder_path='{folder_path}')()
                    mdBtn(onclick='{add_task}', color='green', text='create', icon='add')()




    def CSS(self):
        css('.red_border', border='1px solid red')

    def JS(self):
        me.ret=''
    
        


        @on("mount")
        def mount():
            pass

        @make_self
        def parse_form():
            data = {}
            labels = {}
            tags = me.tags.mdtxtinp
            for inp in tags: 
                id_ = inp.opts.id_[9:]
                data[id_] = jQuery('#'+inp.opts.id_).val()
                labels[id_] = inp.opts.label
            labels['']
            data['json_content']=me.ret
            return data

        @make_self
        def add_task(e):
            data = parse_form()
            print(data)
            Materialize.toast('<h5>Creating task...</h5>',2000, 'green')
            def task_added(res):
                me.update()
            jQuery.post('add_task', {'json' : JSON.stringify(data)}, task_added)  
            me.address='asdas' 
            me.update()         