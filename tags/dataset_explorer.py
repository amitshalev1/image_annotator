
from riotpy import *

class dataset_explorer(RiotTag):
    def HTML(self):
        with div(class_='black white-text', style='margin:1%; width:98%; padding:6px;'):
            h5('Dataset:  {opts.options}')()
            div(id='tsne_echart', style='width:100%; height:800px;')()
            
            #--------------------
            with div(id='image_selection_area'): 
                with div(each='{imgsrc in selected_images}', class_='img_on_stage'):
                    img(src='{imgsrc}', largesrc = '{imgsrc_small_to_large(imgsrc)}', 
                         onmousemove='{sel_img_mouse_move}',
                         onmouseleave='{sel_img_mouse_leave}',
                         onclick='{sel_img_clicked}')()

            #--------------------
            with div(id='feature_learning_div'):
                with row():
                    with div(class_='input-field col s8'):
                        input(id="new_feature_name", type="text")()
                        label(for_="new_feature_name", content='new feature name')()
                    with col('s3'):
                        a(class_='btn waves-effect waves-light', content='+ learn feature', onclick='{learn_feature}')()

            #--------------------
            with div(id='features_list'):
                with row(each='{feature in learned_features}'):
                    with col('s1'):
                        pass
                    with col('s5'):
                        a('{feature}', href='#dataset_feature_learner/{opts.options}&&{feature}')()

            #--------------------
            with div(id='magnifier'):
                img(src='', style='width:100%')()
    
    def CSS(self):
        css('#image_selection_area', float='left', width='100%', padding='10px 5px', display='block')
        css('.img_on_stage', float='left', width='8%', padding='1px')
        css('.img_on_stage img', width='100%', cursor='pointer', border='3px solid black')
        css('.img_on_stage img:hover', border='3px solid white', margin='0')
        css('#magnifier', position='absolute', width='800px', height='600px')


    #==================================================================
    #==================================================================
    #==================================================================
    def JS(self):
        me.selected_images = []
        me.selected_images_large = []
        me.selected_stage = []
        @on("mount")
        def mount():
            load_tsne_visualization()
            me.tsneChart = echarts.init(document.getElementById('tsne_echart'))
            me.draw_tsne_visualization()
            jQuery('#magnifier').hide()
            me.load_learned_feature_list()
            
        
        @make_self
        def load_learned_feature_list():
            def learned_features_loaded(res):
                me.learned_features = res
                me.update()
            jQuery.get('get_all_features/' + me.opts.options, learned_features_loaded)
            
        @make_self
        def load_tsne_visualization():
            def loaded(res):
                me.tsne_data = res
                me.draw_tsne_visualization()
            
            jQuery.get('load_tsne_visualization/'+me.opts.options, loaded, 'json')
        
        @make_self
        def prep_tsne_data():
            if me.tsne_data != undefined:
                me.data, mnx, mxx, mny, mxy = [], 0, 0, 0, 0
                for img, clust, x, y in me.tsne_data:
                    me.data.append({value : [x,y],
                                 name: 'cluster_%s'%(clust),
                                 symbol: 'image://%s'%(img),
                                 symbolSize: [70,55]
                              })
                    if x < mnx: mnx = x 
                    if y < mny: mny = y
                    if x > mxx: mxx = x 
                    if y > mxy: mxy = y 
                return me.data, mnx, mxx, mny, mxy
            return [], 0, 0, 0, 0

        @make_self
        def draw_tsne_visualization():
            data, mnx, mxx, mny, mxy = me.prep_tsne_data()
            def tooltip(params):
                img = params.data.symbol[8:]
                return "<img style='width:400px;', src='"+img+"'></img>"
                
            mnx, mxx, mny, mxy = mnx*1.3, mxx*1.3, mny*1.3, mxy*1.3
            option = {
                xAxis : [{min: mnx, max: mxx}],
                yAxis : [{min: mny, max: mxy}],
                legend: {},
                tooltip: {formatter:tooltip},
                series : {name : 'image tsne', 
                          type : 'scatter',
                          data: data
                         },
                brush: {brushMode: 'multiple', throttleType: 'debounce'},
                dataZoom : [{type: 'inside', xAxisIndex: 0},
                            {type: 'inside', yAxisIndex: 0}],
                toolbox: {show: True, feature: {
                    restore: {show: true, title:'restore'},
                    saveAsImage: {show: true, title:'save as image'},
                    dataZoom: {show:True, title:{zoom: 'zoom', back: 'back'}},
                    brush: {type: ['rect', 'polygon', 'keep', 'clear'], title: {'rect' : 'select rectangle', 'polygon' : 'select polygon', 'keep': 'keep', 'clear': 'clear selection'}}
                 }}
            }
            me.tsneChart.setOption(option)
            me.tsneChart.on('brushselected', me.onbrushSelected)
            me.tsneChart.on('mouseup', me.ontsneChartmouseup)
            me.tsneChart.on('datazoom', me.onDataZoom)

        @make_self
        def onDataZoom(params):
            zx, zy = params.batch
            pass

        @make_self
        def onbrushSelected(items):
            selected_stage = items.batch[0].selected[0].dataIndex
            me.selected_stage = [me.data[i] for i in selected_stage]
            if len(me.selected_stage) > 0:
                 me.ontsneChartmouseup({data: me.selected_stage})
            
        @make_self
        def imgsrc_small_to_large(imgsrc):
            imgsrc = imgsrc.replace('/project_small_', '/project_hashed_images')
            imgsrc = imgsrc.replace('small_224_', '')
            return imgsrc

        @make_self
        def ontsneChartmouseup(e):
    
            if len(me.selected_stage) > 0:
                sel = me.selected_stage
            else:
                sel = [e.data]

            for im in sel:
                imgsrc = im.symbol.split('//')[-1]
                imgsrc_large = imgsrc.split('small_224_')[-1]

                if not imgsrc in me.selected_images:
                    me.selected_images.append(imgsrc)
                    #cache the large image...
                    jQuery('body').append('<img src="'+me.imgsrc_small_to_large(imgsrc)+'" style="display:none">')
            me.update()
        
        @make_self
        def sel_img_mouse_move(e):
            element = jQuery(e.target)
            x, y = e.screenX, e.screenY
            large_imgsrc = e.target.getAttribute('largesrc')
            
            mag =  jQuery('#magnifier')
            magw , magh = mag.width(), mag.height()
            etop, eleft = element.offset().top, element.offset().left
            ewidth, eheight = element.width(), element.height()
            
            top = etop + eheight - magh - 10
            left = eleft + ewidth
            if left > magw: left = 30

            mag.css({'left' : left, 'top' : top})
            mag.find('img').attr('src', large_imgsrc)
            mag.show()
        
        @make_self
        def sel_img_mouse_leave(e):
            jQuery('#magnifier').hide()

        @make_self
        def sel_img_clicked(e):
            src = large_imgsrc = e.target.getAttribute('src')
            jQuery('#magnifier').hide()
            me.selected_images = [im for im in me.selected_images if im !=src]
            me.update()

        @make_self
        def learn_feature():
            feature_name = jQuery('#new_feature_name').val()
            examples = me.selected_images
            dataset = me.opts.options
            
            if feature_name == '':
                Materialize.toast('You must provide a feature name!', 3000, 'red')
            elif len(examples) < 2:
                Materialize.toast('Please select at least 2 examples!', 3000, 'red')
            else:
                Materialize.toast('Starting to learn feature %s for dataset %s'%(feature_name, dataset), 3000, 'green')
                examples = [e.split('small_224_')[1].rstrip('.jpg') for e in examples]
                j = JSON.stringify({'dataset': dataset, 'feature_name' : feature_name, 'examples' : examples})
                def new_feature_added(res):
                    if res == 'feature_created':
                        Materialize.toast('feature created!', 2000, 'green')
                        me.load_learned_feature_list()
                    else:
                        Materialize.toast(res, 2000, 'red')
                jQuery.post('add_new_feature_to_dataset', j, new_feature_added)
            

            
            
            
            