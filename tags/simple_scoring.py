from riotpy import *
class simple_scoring(RiotTag):
    def HTML(self):
        with div(class_='blue-grey darken-2 left', style='width:100%; margin:2px 1px; padding:3px; height:52px;'): 
            with row():
                with col('s3 valign-wrapper yellow-text left-align'):
                    h5("{trait}")()
                with col('s4'):
                    with a(href='get_task_results_per_user/{trait}/{task_id}/{document.current_user}', title='download data', class_='btn-floating left green waves-effect waves-light', style='margin-right:6px; margin-top:8px; width:32px; height:32px; padding-top:7px;'):
                        i(class_='material-icons tiny', content='file_download', style='font-size:20px; line-height:20px;')()
                    h5(class_='truncate', content="{name}")()
                with col('s2 left', style='padding:12px'):
                    span("press a number for the score")()
                with col('s3 valign-wrapper right', style='padding:12px;'):
                    i(class_='material-icons left', content='photo')()
                    div("{annotated}/")()
                    div("{total_images}")()
                    span(style='width:20px;')()
                    with div(class_='progress', style='margib-top:5px;'):
                        div(class_='determinate', style='width:{annotated/total_images * 100}%')()
                
                    a(href='#explore_data/{task_id}', class_='hoverable', style='margin:-8px 3px 0 5px; background:yellow; color:black; padding:3px; border-radius:4px;', content='explore...')()
        
        div(id='magnifier_area', style='width:300px; height:300px; position:absolute; top:100px; left:80px; border:1px solid #555;')()

        with div(id='image_area',  style='float:left; width:100%; height:75%;'):
            img(src='{current_image}', style='height:100%; margin:0 auto; display:block;', data__zoom="{current_image}", id='main_image')()
        
       

        with div(id='scale_area', style='width:100%; height:25%;'):
                with div(each='{val, idx in values}', onclick='{scale_clicked}', style='width:10%; float:left; cursor:pointer;'):
                    img(src='{scale_images[idx]}', style='width:100%; height:100px; border:1px solid grey;', val='{val}', idx='{idx}')()
                    div(content='{val}', style='text-align:center; width:15px; font-size:12px; color:white; margin-top:-90px; margin-left:10px; width:15px; height:15px; background:black; border-radius:10px;', val='{val}')()
    

    def JS(self):
        me.total_images = 0
        me.annotated = 0
        me.current_image = 'img/clear.jpg'
        me.scale_images = []
        @on("mount")

        def mount():
            for k in me.opts.task.keys(): me[k] = me.opts.task[k]
            me.values = me.possible_values.split(',')
            me.scale_images = ['img/clear.jpg' for i in me.values]
            jQuery(window).on('keypress', key_clicked)
            me.load_image_list()
            me.load_scale()
            me.update()

        @make_self
        def load_image_list():
            if not document.current_user:
                Materialize.toast("<h5>Please login first!</h5>", 2000, 'red')
                route('')
            data = JSON.stringify({'task' : me.opts.task, 'user' : document.current_user})
            
            def image_list_loaded(res):
                me.images_pool = res['to_annotate']
                me.annotated = len(res['annotated'])
                me.total_images = len(res['to_annotate']) + me.annotated
                me.current_image = me.images_pool.pop(0)
                me.start_magnifier()
                me.update()

            jQuery.post('simple_scoring_get_image_list', data, image_list_loaded, 'json')
            

        @make_self
        def key_clicked(e):
            if e.key in me.possible_values:
                me.annotate_and_move_to_next(e.key)

        @make_self
        def start_magnifier():
            jQuery('#magnifier_area').hide()
            def onhide():
                jQuery('#magnifier_area').hide()
            
            def onshow():
                jQuery('#magnifier_area').show()

            Drift(document.querySelector('#main_image'), {
                   paneContainer: document.querySelector('#magnifier_area'),
                   showWhitespaceAtEdges: True,
                   onHide: onhide,
                   onShow: onshow})

        @make_self
        def annotate_and_move_to_next(key):
            jQuery(window).off('keypress')
            def annotation_saved(res):
                if res == 'OK':
                    Materialize.toast("<h4>"+key+"</h4>", 1000, 'green')
                    me.scale_images[int(key)] = me.current_image
                    me.current_image = me.images_pool.pop(0)
                    me.annotated += 1
                    me.save_scale()
                    me.start_magnifier()
                    me.update()
                    jQuery(window).on('keypress', key_clicked)
            
            data = JSON.stringify({'task' : me.opts.task, 'user' : document.current_user, 'score' : key, 'image' : me.current_image})
            jQuery.post('annotate_image', data, annotation_saved)

        @make_self
        def scale_clicked(e):
            val = e.target.getAttribute('val')
            annotate_and_move_to_next(val)

        @make_self
        def load_scale():
            def scale_loaded(res):
                if not res == '[]':
                    me.scale_images = res 
                    me.update()   
            jQuery.get('load_scale/%s/%s/%s'%(me.trait, me.task_id, document.current_user), scale_loaded, 'json')
        

        @make_self
        def save_scale():
            def scale_saved(res):
                print(res)
            data = {'user':  document.current_user, 'task_id': me.task_id, 'trait': me.trait, 'scale': me.scale_images}
            jQuery.post('save_scale', JSON.stringify(data), scale_saved)
