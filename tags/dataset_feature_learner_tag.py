from riotpy import *
from tags.bounce_spinner_tag import *

class dataset_feature_learner(RiotTag):
    def HTML(self):
        with div(class_='card blue-grey darken-3 white-text', style='width:95%; margin:1% 2%;'):
            with div(class_='card-content'):
                with span("Learner: {dataset} : {feature}", class_='card-title'):
                    bounce_spinner(style='float:left;', id='learning_spinner')()
                    
                    with a(onclick='{store_and_learn}',class_='btn right amber waves-effect waves-light black-text'):
                        i(class_='material-icons small left', content='wb_sunny')()
                        content('store and learn')
                    
                    span(id='learning_status', content='', class_='right yellow-text', style='margin: 1px 5px')()
                    bounce_spinner(style='float:right;', id='learning_spinner2')()

            with row(id='examples_area', style='padding:0; margin:0;'):
                with col('s6 card green darken-4', id='positive_examples'): 
                    example_box(type='positive', examp = '{examples.predicted_true}', ds='{dataset}')()
                with col('s6 card red darken-4', id='negative_examples'):
                    example_box(type='negative', examp = '{examples.predicted_false}', ds='{dataset}')()
            with row():
                with col('s5'):
                  a(onclick='{delete_feature}', class_='btn red waves-effect waves-light', content='delete feature')()
            
            with row():
                h5("Annotated positive examples:", style='margin:0; padding:0')()
            with row(style='padding:2px;'):
                selected_examples()()

            with row():
                with col('s3'):
                  a(onclick='{turn_to_dataset}', class_='btn green waves-effect waves-light', content='turn to dataset...')()
                with col('s4'):
                    span(class_='white-text', content='(save this feature as a new dataset!)')()


    def JS(self):
        me.examples = {'predicted_false' : [],  'predicted_true': []}
        me.learning_started = False
        
        @on("mount")
        def mount():
            jQuery('#learning_spinner').hide()
            jQuery('#learning_spinner2').hide()
            me.dataset, me.feature = decodeURI(me.opts.options).split('&&')
            me.update()
            me.load_examples()
        
        @make_self
        def load_examples():
            def examples_loaded(res):
                jQuery('#learning_spinner').hide()
                me.examples = {'predicted_false' : [],  'predicted_true': []}
                me.update()
                me.examples = res
                me.update()
                jQuery('.material-tooltip').hide()
                jQuery('.tooltipped').tooltip()
                me.tags.selected_examples.load_positives()

            jQuery('#learning_spinner').show()
            jQuery.get('get_learning_examples/'+me.dataset+'/'+me.feature, examples_loaded)

        @make_self
        def store_and_learn():
            me.animate_remove_objects() 
            def learn_started(res):                
                Materialize.toast(res, 2000)
                setTimeout(me.follow_learning_status, 3000)
            data = JSON.stringify({dataset: me.dataset, feature: me.feature, examples: me.examples})
            jQuery.post('store_and_learn', data, learn_started)
            

        @make_self
        def move_example(example, class_type):
            if class_type == 'positive':
                me.examples['predicted_true']= [v for v in me.examples['predicted_true'] if v!=example]
                me.examples['predicted_false'].append(example)
            else:
                me.examples['predicted_false'] = [v for v in me.examples['predicted_false'] if v!=example]
                me.examples['predicted_true'].append(example)
            jQuery('.tooltipped').tooltip('close')
            jQuery('.tooltipped').tooltip('remove')
            
            me.update()
            
            jQuery('.tooltipped').tooltip()

        @make_self
        def follow_learning_status():
            def check_status(res):
                jQuery('#learning_spinner2').show()
                jQuery('#learning_status').text(res)
               
                if me.learning_started and res == 'ok':
                    me.learning_started = False
                    me.load_examples()
                    jQuery('#learning_spinner2').hide()
                    jQuery('.example_image').show()
                    
                elif res!='ok':
                    me.examples = {'predicted_false' : [],  'predicted_true': []}
                    me.learning_started = True
                    setTimeout(me.follow_learning_status, 2000)
                else:
                    setTimeout(me.follow_learning_status, 2000)
                
                 
            jQuery.get('get_learning_status/'+me.dataset+'/'+me.feature, check_status)

        @make_self
        def delete_feature():
            def is_deleted(res):
                Materialize.toast(res, 3000, 'red')
            jQuery.get('remove_feature/%s/%s'%(me.dataset, me.feature), is_deleted)
        
        @make_self
        def animate_remove_objects():
            objs = jQuery('.example_image')
            for idx, o in enumerate(objs):
                jQuery(objs[idx]).hide(idx*100)
            


#=================================================
class example_box(RiotTag):
    def HTML(self):
        with div(class_='card-content', style='margin:3px 15px; padding:0 5px;'):
            span('predicted {opts.type} examples', class_='center-align card-title white-text')()
            with row():
                with col('s3 example_image  hoverable tooltipped', 
                         each='{ex in opts.examp}',
                         data__position="{opts.type=='positive'?'right':'left'}",
                         data__tooltip="<img style='width:800px;' src='cache/project_hashed_images{opts.ds}/{ex}.jpg'>",
                         data__html='true',
                         data__delay="50",
                         onclick='{sample_clicked}',
                         image_is='{ex}',
                         style='padding:2px;'):
                    img(src='cache/project_small_{opts.ds}/small_224_{ex}.jpg', image_is='{ex}')()
        
    
    def CSS(self):
        css('.example_image', padding='2px;')
        css('.example_image img', width='100%')

    def JS(self):
        @make_self
        def sample_clicked(e):
            jQuery(e.target).tooltip('remove')
            example = e.target.getAttribute('image_is')
            class_type  = me.opts.type
            me.parent.move_example(example, class_type)
        
        





#==========================================================
class selected_examples(RiotTag):
    def HTML(self):
        with col('s1', each='{im in selected_examples}', style='padding:2px;'):
            img(src = '{im}', style='width:100%;')()
    
    def JS(self):
        me.selected_examples = []

        @make_self
        def load_positives():
            def loaded_positives(res):
                me.selected_examples = []
                for v in res:
                    me.selected_examples.append(v[2])
                me.update()
            jQuery.get('get_current_positives/%s/%s'%(me.parent.dataset, me.parent.feature), loaded_positives, 'json')