from riotpy import *
class task_viewer(RiotTag):
    def HTML(self):
        with div(class_='container z-depth-2 black white-text', style = 'width:98%;min-height:90%; '):
            with div(id='task_area'):
                h5("{message}")()
        
    def JS(self):
        me.message = ''

        @on("mount")
        def mount():
            me.task_id  = me.opts.options
            load_task_data(me.task_id)

        @make_self
        def load_task_data(task_id):
            def task_loaded(res):
                if res.type == 'simple scoring': 
                    riot.mount('#task_area', 'simple_scoring', {'task' : res})
                elif res.type == "segmentation":
                    riot.mount('#task_area', 'segmentation_tag', {'task' : res})
                else:
                    me.message = "%s is not implemented yet"%(res.type) 
                    me.update()
                riot.mount('#task_area', 'segmentation_tag', {'task' : res})
            jQuery.get('get_task/'+task_id, task_loaded, 'json')
            
