from riotpy import *


#==================================================
class segmentation_tag(RiotTag):
    def HTML(self):
        with row():
            with col('s2', id='left_menu'):
                seg_objects_toolbox()()
                seg_image_list()()                
                seg_objects_list()()
                
                
                
            with col('s8', id='main_image'):
                seg_image_area(src='img/clear.jpg')()
            
            with col('s2', id='choose'):
                select_object()()
                h3('a              ')()
                select_object_2()()
    
    def CSS(self):
        css('#left_menu', height='100%', border_right='1px solid #777')
    
    def JS(self):
        @on("mount")
        def mount():
            jQuery('#ProjectTitle').html("-" + me.opts.task.name)
            jQuery('#routing_point').css('height', '90%')


'''
 ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄            ▄▄▄▄▄▄▄▄▄▄   ▄▄▄▄▄▄▄▄▄▄▄  ▄       ▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░▌ ▐░░░░░░░░░░░▌▐░▌     ▐░▌
 ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌ ▐░▌   ▐░▌ 
     ▐░▌     ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌▐░▌       ▐░▌  ▐░▌ ▐░▌  
     ▐░▌     ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌          ▐░█▄▄▄▄▄▄▄█░▌▐░▌       ▐░▌   ▐░▐░▌   
     ▐░▌     ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌          ▐░░░░░░░░░░▌ ▐░▌       ▐░▌    ▐░▌    
     ▐░▌     ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░▌       ▐░▌   ▐░▌░▌   
     ▐░▌     ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌▐░▌       ▐░▌  ▐░▌ ▐░▌  
     ▐░▌     ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌ ▐░▌   ▐░▌ 
     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░▌ ▐░░░░░░░░░░░▌▐░▌     ▐░▌
      ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀   ▀▀▀▀▀▀▀▀▀▀▀  ▀       ▀ 
'''


#==================================================            
class seg_objects_toolbox(RiotTag):
    def HTML(self):
        
        with a(title='image done!', id='is_done_btn', is_done='0', onclick='{is_done_btn_clicked}', class_='btn btn-floating right grey darken-3 waves-effect waves-light', style='margin-right:-32px;'):
            i(class_='material-icons', content='done_all')()

        with div(class_='btn black ehite-text', id='object_div', title='{object}', style='width:15px; padding:3px 0; font-size=10px;'):
            content('D')

        with div(id='object_drawing_div', onclick='{add_object}', class_='btn waves-effect waves-light', title='add circular object', style='width:35px; padding-left:8px;'):
            i(class_='material-icons tiny left', content='edit', style='margin:0')()
            span('(N)', style='float:left; margin-left:10px; margin-top:-25px; font-size:10px;')()
        
        with div(id = 'polygons_draw_div', onclick='{parent.tags.seg_image_area.start_polygon_mode}', class_='btn waves-effect waves-light', title='Polygon', style='width:35px; padding-left:8px;'):
            i(class_='material-icons tiny left', content='timeline', style='')()
            span('(P)', style='float:left; margin-left:10px; margin-top:-25px; font-size:10px;')()

        with div(id='object_drawing_circle_div', onclick='{add_object_circle}', class_='btn waves-effect waves-light', title='add circular object', style='width:35px; padding-left:8px;'):
            i(class_='material-icons tiny left', content='radio_button_unchecked', style='margin:0')()
            span('(C)', style='float:left; margin-left:10px; margin-top:-25px; font-size:10px;')()
        
        with div(id = 'object_drawing_rectangle_div', onclick='{add_object_rectangle}', class_='btn waves-effect waves-light', title='add rectangular object', style='width:35px; padding-left:8px;'):
            i(class_='material-icons tiny left', content='check_box_outline_blank', style='margin:0')()
            span('(R)', style='float:left; margin-left:10px; margin-top:-25px; font-size:10px;')()

        '''with div(id = 'magic_points_draw_div', onclick='{parent.tags.seg_image_area.start_magic_points_mode}', class_='btn waves-effect waves-light amber accent-4', title='Magic', style='width:35px; padding-left:8px;'):
            i(class_='material-icons tiny left', content='crop_free', style='')()
            span('(M)', style='float:left; margin-left:10px; margin-top:-25px; font-size:10px;')()'''
        
        

        '''with div(id = 'object_drawing_rectangle_div', onclick='{undo_last_change}', class_='btn waves-effect waves-light', title='UNDO', style='width:35px; padding-left:8px;'):
            i(class_='material-icons tiny left', content='undo', style='')()
            span('(^Z)', style='float:left; margin-left:10px; margin-top:-25px; font-size:10px;')()'''


        with div(onclick='{edit_object}', class_='btn waves-effect waves-light green box_btn_div', id='edit_btn', content='edit (E)', style='padding:0'):
            i(class_='material-icons tiny left', content='tune', style='margin-right:8px;')()
            
        
        
        
        #------------------------------------------
        with div(id='edit_box'):
            with div(class_='edit-btn btn-floating waves-effect waves-light red', onclick='{edit_tool_clicked}'):
                 i(content='delete', title='delete object', class_='material-icons')()
            
            with div(class_='edit-btn btn-floating waves-effect waves-light orange', onclick='{edit_tool_clicked}'):
                 i(content='rotate_left', title='rotate left', class_='material-icons')()
            with div(class_='edit-btn btn-floating waves-effect waves-light orange', onclick='{edit_tool_clicked}'):
                 i(content='rotate_right', title='rotate right', class_='material-icons')()
            
            with div(class_='edit-btn btn-floating waves-effect waves-light purple', onclick='{edit_tool_clicked}'):
                 i(content='linear_scale', title='simplify', class_='material-icons')()
            
            with div(class_='edit-btn btn-floating waves-effect waves-light orange', onclick='{edit_tool_clicked}'):
                 i(content=' photo_size_select_large', title='scale up', class_='material-icons')()
            with div(class_='edit-btn btn-floating waves-effect waves-light orange', onclick='{edit_tool_clicked}'):
                 i(content='photo_size_select_small', title='scale down', class_='material-icons')()
            
            with div(class_='edit-btn btn-floating waves-effect waves-light blue', onclick='{edit_tool_clicked}'):
                 i(content='check_box_outline_blank', title='rectangle', class_='material-icons')()
            with div(class_='edit-btn btn-floating waves-effect waves-light blue', onclick='{edit_tool_clicked}'):
                 i(content='radio_button_unchecked', title='circle', class_='material-icons')()
            

        
        #------------------------------------------
        div(id='zoom_title', content='zoom')()
        with div(class_='range-field'):
            input(onchange='{zoom_changed}', type="range", id="zoom_range", value="1", min="1", max="5", step="0.1", style='margin:2px 5px 20px 5px;')()

        
        

    def CSS(self):
        css('#object_div', width='100%', margin='3px')
        css('.box_btn_div', width='75%', font_size='12px', margin='1px 3px', padding_left='3px', padding_right='5px')
        css('#zoom_title', width='100%', font_size='10px', text_align='center', margin_top='20px')
        css('.highlight_object', border='2px solid yellow')
        css('#edit_box', width='100%', margin='3px')
        css('.edit-btn', margin='3px')
       

    def JS(self):
        @on("mount")
        def mount():
            jQuery('#edit_box').hide(0)
            me.object = me.parent.opts.task.object
            me.update()

        @make_self
        def zoom_changed():
            me.parent.tags.seg_image_area.trigger('zoom_changed')
        
        @make_self
        def add_object(): 
            me.parent.tags.seg_image_area.startPath()
        
        @make_self
        def add_object_circle(): 
            me.parent.tags.seg_image_area.startPathCirc()

        @make_self
        def add_object_rectangle(): 
            me.parent.tags.seg_image_area.startPathRec()

        @make_self
        def close_object(): 
            me.parent.tags.seg_image_area.closePath()
        
        
        @make_self
        def undo_last_change():
            me.parent.tags.seg_image_area.undo()


        @make_self
        def edit_object():
            seg_img = me.parent.tags.seg_image_area
            jQuery('#object_div').removeClass('highlight_object')
            if seg_img.add_path_mode == 1:
                seg_img.closePath()
            seg_img.add_path_mode = 0

            if  jQuery('#edit_btn').hasClass('highlight_object'):
                seg_img.edit_path_mode = 0
                jQuery('#edit_btn').removeClass('highlight_object')
                jQuery('#edit_box').hide('slow')
                for p in seg_img.paths: p.selected = False
            else:
                
                seg_img.editPathMode()

        @make_self
        def edit_tool_clicked(e):
            title = e.target.title
            seg_img = me.parent.tags.seg_image_area
            if   title == 'delete object' : seg_img.delete_item()
            elif title == 'rotate left': seg_img.path.rotate(-5)
            elif title == 'rotate right': seg_img.path.rotate(5)
            elif title == 'simplify': seg_img.path.simplify(2)
            elif title == 'scale up': seg_img.path.scale(1.1)
            elif title == 'scale down': seg_img.path.scale(0.9)
            elif title == 'rectangle': seg_img.make_rectangle(e)
            elif title == 'circle': seg_img.make_circle(e)

        @make_self
        def is_done_btn_clicked(e):
            area = me.parent.tags.seg_image_area
            if area.paths !=[]:
                lst = me.parent.tags.seg_image_list
                status = area.get_is_done_status()
                if status == 0:
                    area.update_is_done_status(1)
                else:
                    area.update_is_done_status(0)
                area.save_image_state()
                def reload_():
                    lst.load_img_list(False)
                setTimeout(reload_, 1000) 
            
'''
 ▄            ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░▌           ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀  ▀▀▀▀█░█▀▀▀▀ 
▐░▌               ▐░▌     ▐░▌               ▐░▌     
▐░▌               ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄      ▐░▌     
▐░▌               ▐░▌     ▐░░░░░░░░░░░▌     ▐░▌     
▐░▌               ▐░▌      ▀▀▀▀▀▀▀▀▀█░▌     ▐░▌     
▐░▌               ▐░▌               ▐░▌     ▐░▌     
▐░█▄▄▄▄▄▄▄▄▄  ▄▄▄▄█░█▄▄▄▄  ▄▄▄▄▄▄▄▄▄█░▌     ▐░▌     
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░▌     
 ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀       ▀                                            
'''            
            

#==================================================            
class select_object_2(RiotTag):
    def HTML(self):
        with div(each='{key in next_meta_keys}', content='{key}:{next_meta[key]}', class_='chip'):
            i(class_="tiny right material-icons", content='close', chip_content='{next_meta[key]}', onclick='{remove_chip}', style='cursor:pointer;margin-top:10px;')()
        with div(id='div_sel1_2'):
            mdSelectInput(id='keys_1_2',label='tag key',items='{obj_keys}')()
        with div(id='div_sel2_2'):
            mdSelectInput(id='vals_1_2',label='tag value',items='{obj_vals}')()        
        with div(id='div_sel3_2'):
            input(id='id_sel3_2',type='number',value='')()
        with div(id='div_sel4_2'):
            label('selected object num :{obj_sel}')()            
        with a(class_='btn-floating btn-large waves-effect waves-light red',onclick='{add_to_next}'):
            i(class_='material-icons',content='add')()
        

    def CSS(self):
        css('#div_sel1_2',margin_top='2px',font_size='12px')
        css('#div_sel2_2',margin_top='2px',font_size='12px')


    def JS(self):
        me.obj_type_dict={'task_id':'numeric',
                          'user': ['amits','daniel'],
                          'crop':['tomato','cucumber'],
                          'plant_part':['leaf','stem'],
                          'shine':'numeric',
                          'parent':'object'
                        }
        me.obj_keys=Object.keys(me.obj_type_dict)
        me.obj_vals=[]
        me.obj_sel=''
        me.wait_for_obj_sel=False
        me.path_to_load=''
        
        @on("mount")
        def mount():       
            jQuery('select').material_select()
            jQuery('#keys_1_2').on('change',selected)
            jQuery('#div_sel3_2').hide()
            jQuery('#div_sel4_2').hide()
            me.get_next_meta()

        @make_self
        def get_next_meta(): 
            me.next_meta_keys=Object.keys(me.parent.tags.seg_image_area.paths_filter)
            me.next_meta=me.parent.tags.seg_image_area.paths_filter
            if me.path_to_load!='':
                me.parent.tags.seg_image_area.change_image(me.path_to_load,2)
            me.update()


        @make_self
        def concat_jsons(j1,j2):
            for key in Object.keys(j2):
                j1[key] = j2[key]
            return j1

        @make_self
        def remove_j2_from_j1(j1,j2):
            temp={}
            for key in Object.keys(j1):
                if key not in Object.keys(j2):
                    temp[key] = j1[key]
            return temp

        @on("add_meta_2")
        def add_meta_2(metadata):
            me.parent.tags.seg_image_area.paths_filter=concat_jsons(me.parent.tags.seg_image_area.paths_filter,metadata)

        @on("remove_meta_2")
        def remove_meta_2(metadata):
            me.parent.tags.seg_image_area.paths_filter=remove_j2_from_j1(me.parent.tags.seg_image_area.paths_filter,metadata)            

        @make_self
        def selected(e): 
            me.tempmeta={}
            me.metakey=e.target.value
            me.obj_vals=me.obj_type_dict[me.metakey]
            if me.obj_vals=='numeric':
                me.wait_for_obj_sel=False
                jQuery('#div_sel2_2').hide()
                jQuery('#div_sel4_2').hide()
                jQuery('#div_sel3_2').show()
                jQuery('#id_sel3_2').on('change',getmeta)
                
            elif me.obj_vals=='object':
                me.wait_for_obj_sel=True
                jQuery('#div_sel2_2').hide()
                jQuery('#div_sel3_2').hide()
                jQuery('#div_sel4_2').show()
            
            else:
                me.wait_for_obj_sel=False
                jQuery('#div_sel2_2').show()    
                jQuery('#div_sel3_2').hide()            
                jQuery('#div_sel4_2').hide()                                          
                jQuery('#vals_1_2').on('change',getmeta)
            
            me.update()
            jQuery('select').material_select()
            

        @make_self
        def getmeta(e):
            me.tempmeta={}
            me.tempmeta[me.metakey]=e.target.value


        @make_self
        def add_to_next(e):
            if me.tempmeta!={}:
                add_meta_2(me.tempmeta) 
                me.get_next_meta()

        @make_self
        def remove_chip(e):
            me.tempmeta={}
            me.tempmeta[e.item.key]=me.next_meta[e.item.key]
            remove_meta_2(me.tempmeta)
            me.get_next_meta()  
#==================================================            
class select_object(RiotTag):
    def HTML(self):
        with div(each='{key in next_meta_keys}', content='{key}:{next_meta[key]}', class_='chip'):
            i(class_="tiny right material-icons", content='close', chip_content='{next_meta[key]}', onclick='{remove_chip}', style='cursor:pointer;margin-top:10px;')()
        with div(id='div_sel1'):
            mdSelectInput(id='keys_1',label='tag key',items='{obj_keys}')()
        with div(id='div_sel2'):
            mdSelectInput(id='vals_1',label='tag value',items='{obj_vals}')()        
        with div(id='div_sel3'):
            input(id='id_sel3',type='number',value='')()
        with div(id='div_sel4'):
            label('selected object num :{obj_sel}')()            
        with a(class_='btn-floating btn-large waves-effect waves-light red',onclick='{add_to_next}'):
            i(class_='material-icons',content='add')()
        

    def CSS(self):
        css('#div_sel1',margin_top='2px',font_size='12px')
        css('#div_sel2',margin_top='2px',font_size='12px')


    def JS(self):
        me.obj_type_dict={'crop':['tomato','cucumber'],
                          'plant_part':['leaf','stem'],
                          'shine':'numeric',
                          'parent':'object'
                        }
        me.obj_keys=Object.keys(me.obj_type_dict)
        me.obj_vals=[]
        me.obj_sel=''
        me.wait_for_obj_sel=False
        
        @on("mount")
        def mount():       
            jQuery('select').material_select()
            jQuery('#keys_1').on('change',selected)
            jQuery('#div_sel3').hide()
            jQuery('#div_sel4').hide()
            me.get_next_meta()

        @make_self
        def get_next_meta(): 
            me.next_meta_keys=Object.keys(me.parent.tags.seg_image_area.next_path_metadata)
            me.next_meta=me.parent.tags.seg_image_area.next_path_metadata
            me.update()
        @make_self
        def paths_choose(item,index): 
            return 'object'+index

        @make_self
        def concat_jsons(j1,j2):
            for key in Object.keys(j2):
                j1[key] = j2[key]
            return j1

        @make_self
        def remove_j2_from_j1(j1,j2):
            temp={}
            for key in Object.keys(j1):
                if key not in Object.keys(j2):
                    temp[key] = j1[key]
            return temp

        @on("add_meta")
        def add_meta(metadata):
            me.parent.tags.seg_image_area.next_path_metadata=concat_jsons(me.parent.tags.seg_image_area.next_path_metadata,metadata)

        @on("remove_meta")
        def remove_meta(metadata):
            me.parent.tags.seg_image_area.next_path_metadata=remove_j2_from_j1(me.parent.tags.seg_image_area.next_path_metadata,metadata)            

        @make_self
        def selected(e): 
            me.tempmeta={}
            me.metakey=e.target.value
            me.obj_vals=me.obj_type_dict[me.metakey]
            if me.obj_vals=='numeric':
                me.wait_for_obj_sel=False
                jQuery('#div_sel2').hide()
                jQuery('#div_sel4').hide()
                jQuery('#div_sel3').show()
                jQuery('#id_sel3').on('change',getmeta)
                
            elif me.obj_vals=='object':
                me.wait_for_obj_sel=True
                jQuery('#div_sel2').hide()
                jQuery('#div_sel3').hide()
                jQuery('#div_sel4').show()
            
            else:
                me.wait_for_obj_sel=False
                jQuery('#div_sel2').show()    
                jQuery('#div_sel3').hide()            
                jQuery('#div_sel4').hide()                                          
                jQuery('#vals_1').on('change',getmeta)
            
            me.update()
            jQuery('select').material_select()
            

        @make_self
        def getmeta(e):
            me.tempmeta={}
            me.tempmeta[me.metakey]=e.target.value


        @make_self
        def add_to_next(e):
            if me.tempmeta!={}:
                add_meta(me.tempmeta) 
                me.get_next_meta()

        @make_self
        def remove_chip(e):
            if e.item.key=='task_id':
                Materialize.toast('cannot remove task id tagging', 4000, 'blue')
            elif e.item.key=='user':
                Materialize.toast('cannot remove user tagging', 4000, 'blue') 
            else:
                me.tempmeta={}
                me.tempmeta[e.item.key]=me.next_meta[e.item.key]
                remove_meta(me.tempmeta)
                me.get_next_meta()  


class seg_image_list(RiotTag):
    def HTML(self):
        with div(id='image_list'):
            with div(each='{im in img_list}',id='{im[0]}', class_='img_link', content='{im[0]}', 
                     n_images='{im[2]}', path='{im[1]}', onclick='{img_selected}'):
                span('({im[2]})', class_='green-text', show='{im[2] > 0}')()
                i(show='{im[3]!=0}', content='done_all', class_='material-icons tiny yellow-text')()
                

        
        with div(id="img_counter"):
            div("{img_list.length} images ")()
            div('{marged_imgs_count()} marked')()
            div('{objects_count()} objects')()
        


    def CSS(self):
        css('#image_list', padding='2px', width='100%', max_height='40%', overflow='auto', margin='0')
        css('.img_link', font_size='12px', line_height='12px;', margin='2px', width='100%', cursor='pointer')
        css('.img_link:hover', text_decoration='underline', color='#90caf9')
        css('.img_link_selected', color='#ffe082')
        css('#img_counter', margin_top='10px', border_top='solid 1px yellow', font_size='12px', border_bottom='solid 1px yellow',)


    def JS(self):
        me.img_list = []


        @make_self
        def load_img_list(go_first=True):
            def imgs_loaded(res):
                me.img_list = res
                me.update()

                #trigger click of the first item in the list...
                if go_first:                    
                    jQuery('.img_link:first').click()
            jQuery.get('get_img_list/'+me.parent.opts.task.task_id, imgs_loaded)
            


        @on("mount")
        def mount():
            me.load_img_list()
            document.saving=False

        @make_self
        def img_selected(e):
            if document.saving:
                setTimeout(lambda: me.img_selected(e), 1000)
            else:
                path = e.target.getAttribute('path')
                n_images = e.target.getAttribute('n_images')
                me.parent.tags.seg_image_area.change_image(path, n_images)
                jQuery('.img_link').removeClass('img_link_selected')
                jQuery(e.target).addClass('img_link_selected')

                       
            
        
        @make_self
        def marged_imgs_count():
            return len([im for im in me.img_list if im[2] > 0])
        
        @make_self
        def objects_count():
            try:
                return sum([im[2] for im in me.img_list])
            except:
                return ''
            
'''
 ██████╗ ██████╗      ██╗       ██╗     ██╗███████╗████████╗
██╔═══██╗██╔══██╗     ██║       ██║     ██║██╔════╝╚══██╔══╝
██║   ██║██████╔╝     ██║       ██║     ██║███████╗   ██║   
██║   ██║██╔══██╗██   ██║       ██║     ██║╚════██║   ██║   
╚██████╔╝██████╔╝╚█████╔╝██╗    ███████╗██║███████║   ██║   
 ╚═════╝ ╚═════╝  ╚════╝ ╚═╝    ╚══════╝╚═╝╚══════╝   ╚═╝  
███████████████████████████████████████████████████████████
'''
#==================================================            
class seg_objects_list(RiotTag):
    def HTML(self):
        with div(id='segmentation_list'):
            with div(each='{obj in current_objs}', class_='obj_link', 
                     onclick='{obj_selected}', obj_num = '{obj.obj_num}'):
                i(show='{obj.selected != true}', class_='material-icons tiny left', content='radio_button_unchecked', style='margin:0; padding:0;')()
                i(show='{obj.selected == true}', class_='material-icons tiny left', content='check_circle', style='margin:0; padding:0; color:#a5d6a7;')()
                a('{obj.name}', class_='obj_link_selected_{obj.selected}', obj_num = '{obj.obj_num}')()


    def CSS(self):
        css('#segmentation_list', padding='2px', width='100%', height='20%', overflow='auto', margin='3px')
        css('.obj_link', font_size='12px;', color='white', cursor='pointer')
        css('.obj_link_selected_true', font_size='12px;', color='#a5d6a7')
        css('.obj_link_selected_', font_size='12px;', color='grey')
        css('.obj_link_selected_false', font_size='12px;', color='grey')
        css('.obj_link:hover', text_decoration='underline')
    
    def JS(self):      
        @on("mount")
        def mount():
            me.current_objs = []


        @on("objects_status_changed")
        def objects_status_changed(paths):            
            me.current_objs = []
            for idx, p in enumerate(paths):
                me.current_objs.append({
                    'name': 'object %s'%(idx+1),
                    'obj_num' : idx,
                    "obj" : p,
                    "selected" : p.selected
                })
            jQuery('select').material_select()
            me.update()

        @on("chng_obj")
        def chng_obj(obj_num):           
            area = me.parent.tags.seg_image_area
            for p in area.paths: p.selected=False
            obj = me.current_objs[obj_num]['obj']
            obj.selected = True
            area.path = obj
            me.trigger("objects_status_changed", area.paths)
            if me.parent.tags.select_object.wait_for_obj_sel:
                me.parent.tags.select_object.obj_sel=obj.data.path_id
                me.parent.tags.select_object.tempmeta={}
                me.parent.tags.select_object.tempmeta[me.parent.tags.select_object.metakey]=obj.data.path_id
                me.parent.tags.select_object.update()
            if me.parent.tags.select_object_2.wait_for_obj_sel:
                me.parent.tags.select_object_2.obj_sel=obj.data.path_id
                me.parent.tags.select_object_2.tempmeta={}
                me.parent.tags.select_object_2.tempmeta[me.parent.tags.select_object.metakey]=obj.data.path_id
                me.parent.tags.select_object_2.update()                



        @make_self
        def obj_selected(e):  
             chng_obj(e.target.getAttribute('obj_num'))


         

'''   
 ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌
▐░▌       ▐░▌▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌
▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀█░█▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌
▐░▌       ▐░▌▐░▌     ▐░▌  ▐░▌          ▐░▌       ▐░▌
▐░▌       ▐░▌▐░▌      ▐░▌ ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌       ▐░▌
▐░▌       ▐░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌
 ▀         ▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀ 
'''

#==================================================            
class seg_image_area(RiotTag):
    def HTML(self):
        canvas(id='main_img_canvas', resize="true",  mousewheel='{mousewheel_handler}')()
        with div(id='seg_img_loading', class_='progress'):
            div(class_='indeterminate')()
        

    
    def CSS(self):
        css('#main_img_canvas', width='100%')

    
    def JS(self):
        me.add_path_mode = 0
        me.edit_path_mode = 0
        me.add_circle_mode = 0
        me.magic_points_mode = 0
        me.draw_polygon_mode = 0
        me.deleted_items = []
        me.paths = []
        me.paths_metadata=[]
        paper.settings.handleSize = 6
        me.hitOptions = { segments: true, stroke: true, fill: true, tolerance: 3}
        me.undo_data = {paths : [], deleted_items : []}
        me.paths_filter={'task_id':me.parent.opts.task.task_id,'user':jQuery('loginbox .btn').text().split('logout')[1]}
        a=Date(jQuery.now()) 
        me.next_path_metadata={"task_id":me.parent.opts.task.task_id,"user":jQuery('loginbox .btn').text().split('logout')[1]}

        @on("mount")
        def mount():
            me.hide_loader()
            me.src = me.opts.src
            me.canvas = document.getElementById('main_img_canvas')
            paper.setup(me.canvas)
            me.view = paper.project.view

            jQuery(document).on('keyup', me.canvas_key_press)

        @make_self
        def hide_loader():
            jQuery('#seg_img_loading').hide()
            jQuery('#main_img_canvas').show()

        @make_self
        def show_loader():
            jQuery('#seg_img_loading').show()
            jQuery('#main_img_canvas').hide()
            
        @make_self
        def add_path_paths(pth):  
            a=Date(jQuery.now())             
            pth.data=me.parent.tags.select_object_2.concat_jsons(pth.data,me.next_path_metadata)
            if 'path_id' not in Object.keys(pth.data):
                pth.data.path_id=jQuery.trim(a.toISOString()+jQuery('loginbox .btn').text().split('logout')[1]+pth.data.task_id)
            me.paths.append(pth)    
             
        
        @make_self
        def update_is_done_status(status):
            but = jQuery('#is_done_btn')
            
            but.attr('is_done', status)
            
            but.removeClass('green grey darken-3 pulse')
            
                
            if status == 0: 
                but.addClass('grey darken-2 z-depth-5 pulse')
            else: 
                but.addClass('green pulse')
            me.update()
            def remove_pulse():
                jQuery('#is_done_btn').removeClass('pulse')
            setTimeout(remove_pulse, 3000)    

            

        @make_self
        def get_is_done_status():
            but = jQuery('#is_done_btn')
            return but.attr('is_done')
            
        #--------------------------------
        @make_self
        def load_img_paths(path):
            me.parent.tags.select_object_2.path_to_load=path
            me.show_loader()
            path = path.replace('\\', '/').split('/')[-1]
            task = me.parent.opts.task
            me.infilter=[]
            me.inmeta=[]
            me.outfilter=[]
            me.outmeta=[]
            me.other_paths=[]
            filter_keys=Object.keys(me.paths_filter)
            def paths_loaded(j):       
                for i in range(len(j['paths'])):
                    con=[]
                    for q in filter_keys:
                        con.append(j['paths'][i][1]['data'][q]==me.paths_filter[q])
                    is_is_filter=all(con)

                    if is_is_filter:
                        me.infilter.append(j['paths'][i])
                        me.inmeta.append(j['paths'][i][1]['data'])

                    else:
                        me.outfilter.append(j['paths'][i])
                        me.outmeta.append(j['paths'][i][1]['data'])

                if 'is_done' in j.keys():
                    me.update_is_done_status(j['is_done'])
                        
                for p in me.infilter:
                    if len(p) > 0:
                        me.path = paper.Path(p[1])
                        me.path.selected= False
                        me.path.fillColor = paper.Color(38, 166, 154, 0.1)
                        me.path.strokeColor = 'blue'
                        me.path.closed = True
                        me.paths.push(me.path)
                        me.add_path_events(me.path)
                        
                me.other_paths=me.outfilter
                me.hide_loader()
                me.objects_status_changed()
            jQuery.get('load_paths_of_image/%s/%s'%(task.task_id, path), paths_loaded)

        @make_self
        def add_path_events(path):
            path.onClick = me.pathClicked
            path.onMouseDrag = me.pathDragged
            path.onMouseUp = me.pathMouseUp
            path.onMouseDown = me.pathmouseDown
            
        @make_self
        def img_loaded():
            w, h = me.img.width, me.img.height                
            me.canvas.height = me.canvas.width * (h/w)
            me.original_ratio = (me.canvas.width / w)  
            me.img.scale(me.original_ratio)
            me.img.position.x =  (me.canvas.width/2) #center position
            me.img.position.y =  (me.canvas.height/2) #center position
            me.hide_loader()
            me.view.bounds.width = me.canvas.width
            me.view.bounds.height = me.canvas.height
            
                
                


        def fix_path(path):
            if  path[0] != '/': path = '/'+path
            return path

        @make_self
        def reset_obj_list():
            obj_list = me.parent.tags.seg_objects_list
            obj_list.current_objs = []
            obj_list.update()

        @make_self
        def change_image(path, n_images):           
            me.show_loader()
            for p in me.paths:   p.remove()
            if not me.img == undefined:  me.img.remove()
            me.img_path = path
            me.paths = []
            me.inmeta=[]
            me.outmeta=[]
            me.other_paths=[]
            me.deleted_items = []
            
            me.img = paper.Raster(fix_path(path))

            paper.project.view.zoom = 1
            jQuery('#zoom_range').val(1)
            
            me.update_is_done_status(0)
            me.reset_obj_list()
            #EVENTS
            
            me.img.on('load', me.img_loaded)
            me.view.on('mousedrag', me.mouse_drag)
            me.view.on("mousedown", me.mousedown)
            me.view.on("mouseup", me.mouseup)
            me.view.on("doubleclick", me.doubleclick)
            
            me.inmeta=[]
            me.outmeta=[]
            me.other_paths=[]            
            me.paths = []
            me.current_object = 0
            
            #if thre are stored paths, load them...
            if n_images > 0:
                me.load_img_paths(path)
            me.trigger('zoom_changed')


        @on("zoom_changed")
        def zoom_changed(e):
            paper.project.view.zoom = jQuery('#zoom_range').val()
            

        '''
 ██████╗ █████╗ ███╗   ██╗██╗   ██╗      ███████╗██╗   ██╗███████╗███╗   ██╗████████╗███████╗
██╔════╝██╔══██╗████╗  ██║██║   ██║      ██╔════╝██║   ██║██╔════╝████╗  ██║╚══██╔══╝██╔════╝
██║     ███████║██╔██╗ ██║██║   ██║      █████╗  ██║   ██║█████╗  ██╔██╗ ██║   ██║   ███████╗
██║     ██╔══██║██║╚██╗██║╚██╗ ██╔╝      ██╔══╝  ╚██╗ ██╔╝██╔══╝  ██║╚██╗██║   ██║   ╚════██║
╚██████╗██║  ██║██║ ╚████║ ╚████╔╝██╗    ███████╗ ╚████╔╝ ███████╗██║ ╚████║   ██║   ███████║
 ╚═════╝╚═╝  ╚═╝╚═╝  ╚═══╝  ╚═══╝ ╚═╝    ╚══════╝  ╚═══╝  ╚══════╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝
                                                                                                  
                                                                        
        '''          

        @make_self
        def mousewheel_handler(e):
            console.log(e)
            dy = e.deltaY
            mx = float(jQuery('#zoom_range').attr('max'))
            mn = float(jQuery('#zoom_range').attr('min'))
            step = float(jQuery('#zoom_range').attr('step'))
            val = float(jQuery('#zoom_range').val())
            if dy < 0: new_val = val + step
            else: new_val = val - step
            if (new_val <= mx) and (new_val >= mn):
                jQuery('#zoom_range').val(new_val)
                me.trigger('zoom_changed', e)
            
                me.adjust_bounds_on_drag()
                me.view.bounds.width = me.canvas.width
                me.view.bounds.height = me.canvas.height
        #--------------------------------
        @make_self
        def canvas_key_press(e):
            
            if e.key in ['N', 'n']:
                if not jQuery('#object_div').hasClass('highlight_object'):
                    jQuery('#object_drawing_div').click()
            if e.key in ['C', 'c']:
                if not jQuery('#object_div').hasClass('highlight_object'):
                    jQuery('#object_drawing_circle_div').click()

            if e.key in ['R', 'r']:
                if not jQuery('#object_div').hasClass('highlight_object'):
                    jQuery('#object_drawing_rectangle_div').click()

            '''if e.key in ['M', 'm']:
                if not jQuery('#object_div').hasClass('highlight_object'):
                    jQuery('#magic_points_draw_div').click()'''

            if e.key in ['P', 'p']:
                if not jQuery('#object_div').hasClass('highlight_object'):
                    jQuery('#polygons_draw_div').click()


            if e.key in ['E', 'e']:
                    jQuery('#edit_btn').click()


            if e.key == 'Delete':
                me.delete_item()

            if (e.ctrlKey and e.keyCode == 90):
                me.undo() 
                
        @make_self
        def adjust_bounds_on_drag():
            zoomval = float(jQuery('#zoom_range').val())
            
            bounds = paper.view.bounds
            if bounds.x < 0: 
                paper.view.center = paper.view.center.subtract(paper.Point(bounds.x, 0))
            if bounds.y < 0: 
                paper.view.center = paper.view.center.subtract(paper.Point(0, bounds.y))
            
            bounds = paper.view.bounds
        

            w = bounds.x + bounds.width 
            h = bounds.y + bounds.height
            if zoomval > 2:
                h = bounds.y + bounds.height - 100
            if w > paper.view.viewSize.width:
                    paper.view.center = paper.view.center.subtract(paper.Point(w - paper.view.viewSize.width, 0))
            if h > paper.view.viewSize.height: 
                paper.view.center = paper.view.center.subtract(paper.Point(0, h - paper.view.viewSize.height))
    
        @make_self
        def mouse_drag(e):
            
            # PATH / POLYGON                  
            if (me.add_path_mode == 1) or (me.draw_polygon_mode == 1):
                me.path.add(e.point)

            # EDIT
            #elif me.edit_path_mode == 1:
            #    for p in me.paths: p.selected = False
            
            # CIRCLE  O ----------------------------
            elif me.add_circle_mode == 1:
                
                if me.temp_circle != undefined:
                    me.temp_circle.remove()
                    me.temp_circle = undefined
                
                dx, dy = (e.point.x - me.circle_point.x), (e.point.y - me.circle_point.y) 
                radius = Math.sqrt(dx*dx + dy*dy)
               
                me.temp_circle = paper.Path.Circle(me.circle_point, radius)
                me.temp_circle.strokeColor = 'red'

            # RECTANGLE  [] ----------------------------
            elif me.add_rectangle_mode == 1:
                
                if me.temp_rect != undefined:
                    me.temp_rect.remove()
                    me.temp_rect = undefined
                dx, dy = (e.point.x - me.point.x), (e.point.y - me.point.y) 
                
                size = paper.Size(dx, dy) 
                me.temp_rect = paper.Path.Rectangle(me.point, size)
                me.temp_rect.strokeColor = 'red'

            #  ZOOM  (+)-----------------------    
            else:
                   
                point = paper.view.projectToView(e.point)
                last = paper.view.viewToProject(me.lastPoint)
                paper.view.scrollBy(last.subtract(e.point))
                me.lastPoint = point

                dx, dy = e.delta.x, e.delta.y 
                px, py = paper.view.center.x, paper.view.center.y
                paper.view.setCenter((px - dx, py - dy))

                

                #avoid panning too much down or right ...
                me.adjust_bounds_on_drag()
                
                


        @make_self
        def mousedown(e):  
            me.lastPoint = paper.view.projectToView(e.point)
            if me.add_path_mode == 1: me.path.add(e.point) 
            
            elif me.add_circle_mode == 1:  me.circle_point = e.point
            
            elif me.add_rectangle_mode == 1: me.point = e.point
            
            elif  me.draw_polygon_mode == 1: 
                if me.polygon_start_point == []: 
                    me.polygon_start_point = e.point
                    me.path.add(e.point)
                else:
                    p1, p2 = me.polygon_start_point, e.point
                    distance = Math.sqrt((p1.x - p2.x)**2 + ([p1.y - p2.y])**2)
                    if distance < 4: me.closePath(e)
                    else: me.path.add(e.point)
                

        @make_self
        def mouseup(e):
            if me.add_path_mode == 1 or me.add_circle_mode == 1 or me.add_rectangle_mode == 1:
                me.closePath(e)
                if me.temp_circle != undefined:
                    me.temp_circle.remove()
                    me.temp_circle = undefined
            
            if me.magic_points_mode == 1:
                add_magic_point(e.point)
        @make_self
        def doubleclick(e):
            if me.draw_polygon_mode == 1:
                me.closePath(e)
            

        @make_self
        def closePath(e):
            jQuery('#object_div').removeClass('highlight_object')
            try:
                if (me.add_path_mode == 1) or (me.draw_polygon_mode == 1):

                    me.path.closed = True
                    if me.add_path_mode == 1:
                        me.path.smooth()
                        me.path.simplify(0.4)
                    me.add_path_mode = 0
                    me.draw_polygon_mode = 0
                    me.path.fillColor = paper.Color(38, 166, 154, 0.1)

                    #add events to the path...
                    me.add_path_events(me.path)
                    me.save_image_state()
                
                elif me.add_circle_mode == 1:
                    me.add_circle_mode = 0
                    dx, dy = e.delta.x, e.delta.y 
                    radius = Math.sqrt(dx*dx + dy*dy)
                
                    me.path = paper.Path.Circle(me.circle_point, radius)
                    me.add_path_paths(me.path) 
                    me.path.strokeColor = 'blue'
                    me.path.fillColor = paper.Color(38, 166, 154, 0.1)                    
                    me.add_path_events(me.path)
                    me.save_image_state()

                elif me.add_rectangle_mode == 1:
                    
                    if me.temp_rect != undefined:
                        me.temp_rect.remove()
                        me.temp_rect = undefined

                    me.add_rectangle_mode = 0
                    size = paper.Size(e.delta.x, e.delta.y) 
                    me.path = paper.Path.Rectangle(me.point, size)
                    me.add_path_paths(me.path) 
                     
                    me.path.strokeColor = 'blue'
                    me.path.fillColor = paper.Color(38, 166, 154, 0.1)
                    me.add_path_events(me.path)
                    me.save_image_state()
                
                me.editPathMode() # (???)
                me.path = me.paths[-1]
                me.path.selected = True
                me.objects_status_changed()
                
            except Exception as e:
                print('problem in closePath:', e)

        @make_self
        def initializeModes():
            if me.edit_path_mode == 1:  me.exitEditPathMode()
            me.edit_path_mode = 0
            me.add_path_mode = 0
            me.add_circle_mode = 0
            me.add_rectangle_mode = 0
            me.magic_points_mode = 0
            me.draw_polygon_mode = 0
        
        @make_self
        def start_polygon_mode():
            if me.edit_path_mode == 1: 
                me.exitEditPathMode()
            me.initializeModes()
            me.highlightDrawingMode()
            me.draw_polygon_mode = 1
            me.path = paper.Path()
            me.add_path_paths(me.path)  
            me.path.strokeColor = 'blue'
            me.polygon_start_point = []


        @make_self
        def draw_polygon_point(e):
            pass

        @make_self
        def start_magic_points_mode():
            Materialize.toast('Mark 4 points to auto detect object', 4000, 'blue')
            if me.edit_path_mode == 1: 
                me.exitEditPathMode()
            me.initializeModes()
            me.magic_points_mode = 1

        @make_self
        def add_magic_point(e):
            if me.magic_points == undefined: 
                me.magic_points = []
                me.all_magic_points = []

            if len(me.magic_points) < 4:
                me.magic_points.append((e.x, e.y))

                circ = paper.Path.Circle((e.x, e.y), 2)
                circ.closed = True
                circ.strokeColor = 'blue'
                circ.fillColor = 'blue'
                me.all_magic_points.append(circ)
                
            
            if len(me.magic_points) == 4:
                me.call_magic_points_service()
        
        @make_self
        def add_magic_points_polygon(points):
            segments = [paper.Point(p) for p in points]
            me.path = paper.Path(segments)
            me.add_path_paths(me.path) 
            me.path.strokeColor = 'blue'
            me.path.fillColor = paper.Color(38, 166, 154, 0.1)
            me.path.closed = True
            me.add_path_events(me.path)
            me.save_image_state()

        @make_self
        def call_magic_points_service():
            for p, pp in enumerate(me.magic_points):
                for c, cc in enumerate(pp):
                    me.magic_points[p][c] = int(me.magic_points[p][c]  / me.original_ratio)
            data = {'points' : me.magic_points, 'image_path' : me.img_path}
            
            Materialize.toast('<img src="img/loadinfo.net.gif" style="float:left; margin-right:20px;"/> Finding borders...', 100000, 'blue waiting_toast rounded')
            
            def dextr_prediction_done(res):
                #remove the toast ...
                jQuery('.toast.waiting_toast').first()[0].M_Toast.remove()
                Materialize.toast(' Creating an object...', 2000, 'green')
                points = []
                for x,y in zip(res[0], res[1]):
                    points.append(  (int(x * me.original_ratio),   int(y * me.original_ratio))  )
                add_magic_points_polygon(points)
                
            jQuery.post('magic_points_dextr', JSON.stringify(data), dextr_prediction_done)  

            me.magic_points = []
            me.magic_points_mode = 0
            
            def remove_point():
                if len(me.all_magic_points) > 0:
                    p = me.all_magic_points.shift()
                    p.remove()
                    setTimeout(remove_point, 4000)

            for p in me.all_magic_points:
                p.strokeColor = 'red'; p.fillColor = 'red'
            if len(me.all_magic_points) < 5:
                setTimeout(remove_point, 4000)
               
        @make_self
        def highlightDrawingMode():
            Materialize.toast('Adding object!', 2000, 'green')
            jQuery('#object_div').addClass('highlight_object')
            jQuery('#edit_btn').removeClass('highlight_object')

        #---------------------------------------------------------------------------
        @make_self
        def startPath():
            if me.edit_path_mode == 1: 
                me.exitEditPathMode()
            me.initializeModes()
            me.highlightDrawingMode()
            me.add_path_mode = 1
            
            me.path = paper.Path()
            me.add_path_paths(me.path) 
            me.path.strokeColor = 'blue'
        
        
        @make_self
        def startPathCirc():
            me.highlightDrawingMode()
            if me.edit_path_mode == 1: 
                me.exitEditPathMode()
            me.initializeModes()
            me.add_circle_mode = 1
            

        @make_self
        def startPathRec():
            
            me.highlightDrawingMode()
            if me.edit_path_mode == 1: 
                me.exitEditPathMode()
            me.initializeModes()
            me.add_rectangle_mode = 1


        #---------------------------------------------------------------------------

        

        @make_self
        def editPathMode():
            
            if me.add_path_mode:
                me.closePath()
            me.path = undefined
            me.add_path_mode = 0
            me.edit_path_mode = 1
            jQuery('#edit_box').show('slow')
            jQuery('#edit_btn').addClass('highlight_object')
            me.view.bounds.width = me.canvas.width
            me.view.bounds.height = me.canvas.height
            Materialize.toast('Edit mode', 3000, 'orange')

        @make_self
        def exitEditPathMode():
            me.edit_path_mode = 0
            jQuery('#edit_btn').removeClass('highlight_object')
            jQuery('#edit_box').hide('slow')
            me.segment_selected = null #clear other paths...
            for p in me.paths: p.selected = False #clear other paths...

        @make_self
        def pathClicked(e):
                if me.edit_path_mode == 1:
                    me.segment_selected = null #clear other paths...
                    for p in me.paths: p.selected = False #clear other paths...
                    
                    me.path = e.target
                    me.path.selected = True
                    me.objects_status_changed()
                    
                    hit = me.path.hitTest(e.point, me.hitOptions)
                    if hit.type == 'stroke':
                        try:
                            location = hit.location
                            me.path.insert(location.index + 1, e.point)
                            me.segment_selected = me.path[location.index + 1]
                            try: me.segment_selected.smooth()
                            except Exception as e: print('proclem smoothing', e)
                            me.save_image_state()
                        except Exception as e:
                            print('problem adding point', e)
                        return


        @make_self
        def pathmouseDown(e):
            if me.edit_path_mode == 1:
                if e.target == me.path:
         
                    hit = me.path.hitTest(e.point, me.hitOptions)
                    if hit.type == 'segment':
                        if e.modifiers.shift:
                            hit.segment.remove()
                            me.save_image_state()
                            return

                        Materialize.toast('point selected', 1000)
                        me.segment_selected = hit.segment
                        return

        @make_self
        def pathMouseUp(e):
            me.save_undo_data()
            e.stopPropagation()
            me.segment_selected = null
            me.save_image_state()

        @make_self
        def pathDragged(e):
            
            if me.edit_path_mode: 
                e.stopPropagation()
                if (e.target == me.path):
                    hit = me.path.hitTest(e.point, me.hitOptions)
                    me.path.selected = True
                    
                    if hit:
                        if hit.type == 'fill':
                            e.target.position.x += e.delta.x
                            e.target.position.y += e.delta.y
                            me.save_image_state()
                            me.path.selected = True
                            
                            return 'OK'
                        
                        if me.segment_selected:
                            me.segment_selected.point.x += e.delta.x
                            me.segment_selected.point.y += e.delta.y
                            me.save_image_state()
                            return 'OK'
                    

        @make_self
        def delete_item(e):      
            if me.edit_path_mode: 
                if me.path.selected:
                    idx = me.paths.index(me.path)
                    me.deleted_items.append(idx)
                    me.paths = [p for i, p in enumerate(me.paths) if i!=idx]
                    me.inmeta = [p for i, p in enumerate(me.inmeta) if i!=idx]
                    me.path.remove()
                    me.save_image_state()


            


        
        @make_self
        def make_rectangle(e):
            if me.path:
                
                x = me.path.bounds.x
                y = me.path.bounds.y
                w = me.path.bounds.width
                h = me.path.bounds.height
                idx = me.paths.index(me.path)
                me.deleted_items.append(idx)
                me.path.remove()
                rec = paper.Path([paper.Point([x,y]), 
                                  paper.Point([x+w,y]),
                                  paper.Point([x+w,y+h]),
                                  paper.Point([x,y+h])
                                   ])
                me.path = rec
                me.add_path_paths(me.path)      
                me.path.closed = True
                me.path.strokeColor = 'blue'

                me.path.fillColor = paper.Color(38, 166, 154, 0.1)

                #add events to the path...
                me.add_path_events(me.path)  
                me.save_image_state()

        @make_self
        def make_circle(e):
            if me.path:
                
                x = me.path.bounds.x
                y = me.path.bounds.y
                w = me.path.bounds.width
                h = me.path.bounds.height
                center = paper.Point([x+(w / 2), y+(h/2)])
                if w > h: radius = w/2
                else: radius = h/2

                idx = me.paths.index(me.path)
                me.deleted_items.append(idx)
                me.path.remove()
                circ = paper.Path.Circle(center, radius)

                me.path = circ
                me.add_path_paths(me.path)         
                me.path.closed = True
                me.path.strokeColor = 'blue'

                me.path.fillColor = paper.Color(38, 166, 154, 0.1)

                #add events to the path...
                me.add_path_events(me.path)
                me.save_image_state() 

        @make_self
        def get_detailed_paths():
            def getContour(path):
                #fix to the image ratio
                r = me.original_ratio
                cont = [path.getPointAt(r) for r in range(path.length)]
                cont = [(p.x / r, p.y / r) for p in cont]
                return cont      
            paths = [p.exportJSON({asString: false, precision: 5}) 
                    for i,p in enumerate(me.paths)]+me.other_paths

            contours = [getContour(p) for i,p in enumerate(me.paths)]
            contours = [c for c in contours if len(c) > 0]
            return paths, contours

        @make_self
        def update_image_list_count_num_of_images(paths):
            img_list = me.parent.tags.seg_image_list.img_list
            img = me.img.image.currentSrc.split('/')[-1]
            img_idx = [i for i,m in enumerate(img_list) if m[0] == img][0]
            img_list[img_idx][2] = len(paths)
            me.parent.tags.seg_image_list.update()
            



        @make_self
        def save_image_state():       
            document.saving=True
            task = me.parent.opts.task            
            paths, contours = me.get_detailed_paths()
            is_done=me.get_is_done_status()
            if paths==[] and is_done==1:
                is_done=0
            me.update_is_done_status(is_done)
       

            to_save = {'img' :{"file_address": me.img.image.currentSrc,  'img_width' : me.img.width, 'img_height' : me.img.height,'is_done':is_done},
                       'paths' : paths,
                       'contours' : contours
                      }

            def saved(res):
                if res == 'saved':
                    me.update_image_list_count_num_of_images(paths)
                    me.objects_status_changed()
                document.saving=False


            jQuery.post('save_segmentation_task', JSON.stringify(to_save), saved)

        
        @make_self
        def objects_status_changed():
            obj_list = me.parent.tags.seg_objects_list
            obj_list.trigger('objects_status_changed', me.paths)

            
                    
            
        @make_self
        def undo(e):
            Materialize.toast('Not implemented yet', 2000, 'red')
            '''if not len(me.undo_data['paths']) == 0:
                for p in me.paths: p.remove()
                paths = [p for p in me.undo_data['paths']]
                for p in paths:
                    print(p)
                    me.path = paper.Path(list(p.segments))
                    me.path.closed = True
                    me.path.fillColor = p.fillColor
                    me.path.strokeColor = p. strokeColor
                    me.add_path_events(me.path)

                me.paths = [p for p in paths]
                me.deleted_items = list(me.undo_data['deleted_items'])
                me.undo_data['paths'] = []
                me.undo_data['deleted_items'] = []'''

                #me.save_image_state()

        @make_self
        def save_undo_data():
            me.undo_data['paths'] = [p for p in me.paths]
            me.undo_data['deleted_items'] = [p for p in me.deleted_items]
      
class mdSelectInput(RiotTag):
    def HTML(self):
        with col('{opts.col_class} input-field'):
                with select(id='myid'):
                    option(value='', content="{opts.label}", disabled='', selected='', style='color:grey')()
                    option(each = "{item in opts.items}",id='{item}', value = '{item}', content='{item}')() 
                label("{opts.label}")()