from riotpy import *
class explore_data(RiotTag):
    def HTML(self):
        with div(class_='container black white-text', style='width:95% margin:0; padding:10px;'):
            with h5("Exploring images from task:"):
                div(class_='btn orange right', content='resample', onclick='{random_select}')()

            with row():
                img(class_='small_img', each='{path in samp_images}', data__zoom="{path}", src='{path}', style=' width:20%; float:left;')()
        
        div(id='magnifier_area', style='width:300px; height:300px; position:absolute; top:100px; left:80px; border:1px solid #555;')()
            

    def JS(self):
        me.samp_images = []
        me.samp_size= 20 
        @on("mount")
        def mount():
            me.random_select()

        @make_self
        def random_select():
            me.samp_images = []
            me.update()
            def images_loaded(res):
                me.samp_images = res
                me.update()
                me.start_magnifier()
            jQuery.get('/sample_image/%s/%s'%(me.opts.options, me.samp_size), images_loaded)

        @make_self
        def start_magnifier():
            jQuery('#magnifier_area').hide()
            def onhide():
                jQuery('#magnifier_area').hide()
            
            def onshow():
                jQuery('#magnifier_area').show()

            imgs = jQuery('.small_img')
            for idx in imgs:
                img_element = imgs[idx]
                Drift(img_element, {
                        paneContainer: document.querySelector('#magnifier_area'),
                        showWhitespaceAtEdges: True,
                        onHide: onhide,
                        onShow: onshow,
                        zoomFactor:5,
                        hoverBoundingBox:True})
