import pytest
import time
from tasks import *
from segmentation import *
def test_task_tag():
    #load tasks
    original_number_of_tasks=len(get_tasks())

    #create task
    now_time=time.time()
    mytask={'name':now_time , 'img_source': '/T/experiment_469/task_48/plot_7/', 'type': 'Detection/Segmentation', 'ano': ['alaam'], 'target': ['Hazera'], 'user_name': 'amits', 'users': 'amits'}
    #add
    add_task(mytask)
    #check that task is saved right by loading saved and checking last is equel
    assert get_tasks()[-1]['name']==now_time
#     assert {key:get_tasks()[-1].get(key) for key in get_tasks()[-1].keys() if key!='task_id'}==mytask
    #delete last task
    #perform tests on task:
    
    #load imgs
    
    #delete task
    task = get_task_by_id(get_tasks()[-1]['task_id'])
    assert len(get_image_list_from_path(task))==45
    remove_task(get_tasks()[-1]['task_id'])
    assert original_number_of_tasks==len(get_tasks())
   