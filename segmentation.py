from flask import Blueprint, request, jsonify, send_file
import json
from tasks import *
import pandas as pd
import numpy as np
import os
import time
from tasks import get_task_by_id
from coco_utils import segmentation_task_json_zip, segmentation_task_json_zip_local_store
import ntpath
from phenomics import *


SCORING_DIR = '../results/'
IMAGE_ENDS = ['jpg', 'png', 'gif', 'tif', 'iff', 'tiff']
TASKS_FILE = 'data/tasks.json'
segmentationBP = Blueprint('segmentationBP', __name__)

#===================================================
# UTILITY FUNCTIONS
#===================================================
def fix_path(path):
    if not path[-3:] in ['csv', 'tab', 'tsv', 'lsx', 'txt']:
      if os.name == 'nt': #on windows...
          path = path.replace('/', '\\').replace('\\T', 'T:')  
      if os.path.isdir(path):
            if not path[-1] in ['/', '\\']:
                if os.name == 'nt': 
                    path = path+'\\'
                else: 
                    path = path+'/'
    return path


def get_task_image_path(img_name, task_id):
    task_dir = SCORING_DIR+'annotation_task_%s/'%(task_id)
    img_name =  img_name.replace('\\','/').split('/')[-1]
    fname = task_dir + img_name + '.json'
    return fname

def generate_mask(task_dir, j):
    img_file = j['img'].split('/')[-1], 
    w, h = j['img_width'], j['img_height']
    mask_dir = task_dir + '/masks/'
    img = np.zeros((h, w, 3), np.uint8)
    
    if not os.path.isdir(mask_dir):
        os.mkdir(mask_dir)
    
    for cont in j['contours']:
        pass
        
    mask_fname = mask_dir + img_file+"_mask_.jpg"

    
def combine_task_to_file(task_id):
    task_dir = SCORING_DIR+'annotation_task_%s/'%(task_id)
    def read_json(task_dir, f):
        with open(task_dir+f) as jfile: return json.load(jfile)

    jsons = [read_json(task_dir, f) for f in os.listdir(task_dir) if f.endswith('json')]
    for j in jsons:
        generate_mask(task_dir, j)

    df = pd.DataFrame(jsons)
    
    tabfile = SCORING_DIR+'annotation_task_%s/combined_task_%s.tab'%(task_id, task_id)
    df.to_csv(tabfile, index=False, sep='\t')
    return tabfile
#===================================================    
def get_images_under_directory(directory):
    import re
    pattern = re.compile('.*\.(jpg|JPG|png|gif|tif|iff|tiff)$')    
    matches = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in filter(lambda name:pattern.match(name),filenames):
            matches.append(os.path.join(root, filename))
    return matches

# def get_task_images(task_id):
#     try:
#         res=pd.DataFrame(p.get_images_by_experiment_id(task_id)).image_uri.values.tolist()
#     except:
#         return []   
#     res=['/T/'+x.split('//')[1] for x in res]

#     return res
#===================================================

def extract_paths_num(img_name, task_id):
    fname = get_task_image_path(img_name, task_id)
    if os.path.isfile(fname):
        with open(fname, 'r') as f:
            j = json.load(f)
            paths = [p for p in j['contours'] if len(p) > 0]
            is_done = j.get('is_done', 0)
            return [len( paths ), is_done]
    return [0, 0]

def extract_is_done(img_name, task_id):
    fname = get_task_image_path(img_name, task_id)
    if os.path.isfile(fname):
        with open(fname, 'r') as f:
            j = json.load(f)
            is_done = j.get('is_done', 0)
            return [is_done]
    return [0]

def get_images_under_directory(directory):
    import re
    pattern = re.compile('.*\.(jpg|JPG|png|gif|tif|iff|tiff)$')    
    matches = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in filter(lambda name:pattern.match(name),filenames):
            matches.append(os.path.join(root, filename))
    return matches

def get_image_list_from_path(task_id):
#     path = fix_path(path)
    imgs = [[ntpath.basename(f), f] + extract_paths_num(ntpath.basename(f), task_id['task_id'])
                for f in get_images_under_directory(task_id['img_source'])]
    return imgs
        
    
def check_if_empty(task_id):
    res=get_image_list_from_path(get_task_by_id(task_id))
    if res!=[]:
        return len(res)
    return 'No dir'    


def load_paths_of_image(img_name, task_id):
    with open(fname, 'r') as f:
        return json.load(TASKS_FILE)['']
    return []

    
def save_annotation(anno):
    with open(TASKS_FILE, 'w') as f:
        json.dump(anno, f)
    # task_dir = SCORING_DIR+'annotation_task_%s/'%(anno['task_id'])
    # if not os.path.isdir(task_dir):
    #     os.mkdir(task_dir)
    # user=anno['user'].strip()
    # file_add='/mnt/gluster/catalog/'+anno['img'][anno['img'].find('/T/') +3:]
    # r=dummy.get_image_id_from_uri(file_add)
    # if r.status_code!=200:
    #     print(r)
    #     print('failed trying again')
    #     dummy.renew_auth()
    #     r=dummy.get_image_id_from_uri(file_add)
    #     print('OK')
    # image_id=json.loads(r.text)['image_id']
    # anno['img']=file_add
    # anno['image_id']=image_id
    # fname = get_task_image_path( anno['img'], anno['task_id'])
    # abs_fname=fname.replace('../results','/mnt/gluster/annotations')
    # with open(fname, 'w') as f:
    #     json.dump(anno, f)
    # r=dummy.post_set_image_data_by_inner_key(image_id,json.dumps({'json_adress':abs_fname}),inner_key=f"annotator.{user}") 
    # r=dummy.post_set_image_data_by_inner_key(image_id,json.dumps({'json_content':anno}),inner_key=f"annotator.{user}")
    # if r.status_code!=200:
    #     print('failed trying again')
    #     dummy.renew_auth()
    #     r=dummy.post_set_image_data_by_inner_key(image_id,json.dumps({'json_adress':abs_fname}),inner_key=f"annotator.{user}")
    #     print(r.text)

    

    
        
    

#====================== API ===========================

@segmentationBP.route('/get_img_list/<task_id>')
def get_img_list(task_id):
    
    task = get_task_by_id(task_id)
    return jsonify(get_image_list_from_path(task))

@segmentationBP.route('/is_empty/<task_id>')
def is_empty(task_id):
    return jsonify(check_if_empty(task_id))

@segmentationBP.route('/save_segmentation_task', methods = ['POST'])
def save_segmentation_task():
    anno = request.get_json(force=True)
    save_annotation(anno)
    return 'saved'


@segmentationBP.route('/load_paths_of_image/<task_id>/<img_name>')
def load_paths_of_image_api(img_name, task_id):
    return jsonify(load_paths_of_image(img_name, task_id))


@segmentationBP.route('/combine_task_to_file/<task_id>')
def combine_task_to_file_api(task_id):
    fname = combine_task_to_file(task_id)
    return 'ok'
    return send_file(fname,
                    mimetype='text/tab-separated-values',
                    as_attachment=True)

@segmentationBP.route('/download_task_coco_format_results_as_zip/<task_id>')
def download_task_coco_format_results_as_zip(task_id):
    zfile = segmentation_task_json_zip(task_id=task_id) 
    return send_file(zfile, mimetype='application/zip', as_attachment=True)


@segmentationBP.route('/store_task_coco_format_results_as_zip/<task_id>')
def store_task_coco_format_results_as_zip(task_id):
    import multiprocessing
    proc = multiprocessing.Process(target=segmentation_task_json_zip_local_store, args=(task_id,))
    proc.start()
    zfile = segmentation_task_json_zip_local_store(task_id=task_id) 
    return str(proc.pid)