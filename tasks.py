from flask import Blueprint, request, jsonify
import json, os
from phenomics import *
import subprocess
import time


ROOT_DIR='/T/'
TASKS_FILE = 'data/tasks.json'
FOLDER_TREE='foldertree.json'
FOLDER_TREE_2='foldertree2.json'

# temp1=pd.DataFrame(pheno.get_experiments().experiment_id)
# temp1.columns=['task_id']    
# tasks_=list(temp1.T.to_dict().values())
# direc='/T/'
tasksBP = Blueprint('tasksBP', __name__)

#--------------------------- Helper functions --------------------------------
# def get_experiments(p):
#     return p.get_experiments().experiment_id.values.tolist()

def save_tasks(tasks):
    '''function to push tasks into the TASKS_FILE'''
    with open(TASKS_FILE, 'w') as f:
        f.write(json.dumps(tasks))

def get_tasks():

    '''an helper function to load all tasks from TASKS_FILE...'''
    try:
        with open(TASKS_FILE) as file:
            return json.load(file)
    except FileNotFoundError:
        save_tasks([])
        return []

def get_next_task_id(tasks):
    '''helper function to return the max task_id+1'''
    ids = [int(u['task_id']) for u in tasks]
    if not ids:
        return 0
    else: return max(ids) + 1

def add_task(task_dict):
    '''Adds a task to the tasks database'''
    with open(TASKS_FILE, 'w') as f:
        f.write(json.dumps(task_dict))
    return task_dict


def remove_task(task_id):
    '''Adds a task to the tasks database'''
    tasks = [t for t in get_tasks() if int(t['task_id']) != int(task_id)]
    path = f'data/results/annotation_task_{task_id}'
    dst = f'data/results/dep/annotation_task_{task_id}'
    if os.path.isdir(path):
        os.rename(path, dst)  
    save_tasks(tasks)
    return tasks


def get_task_by_id():
    '''returns the task associated with a task_id'''
    with open(TASKS_FILE) as file:
        return json.load(file)

#-------------- claculating stats of segmentation tasks -----------------
def task_get_json_files(task_id):
    path = f'data/results/annotation_task_{task_id}'
    if os.path.isdir(path):
        return [f'{path}/{f}'  for f in os.listdir(path) if f.endswith('json')]
    else:
        return None
    
def task_count_objects(json_file):
    with open(json_file, 'r') as f:
        return len([p for p in json.load(f)['contours'] if len(p) > 0])

def task_count_files_and_objects(task_id):
    files = task_get_json_files(task_id)
    try:  n_objects = sum([task_count_objects(json_file) for json_file in files])
    except TypeError: n_objects = 0
    try: anno_imgs = len(files)
    except: anno_imgs = 0
    return {'task_id' : task_id, 'anno_imgs' : anno_imgs, 'n_objects' : n_objects}
    
    
def get_segmentation_tasks_stats():
    '''returns a list of dicts stating the number of images and segmented objects is each task'''
    seg = [t['task_id'] for t in get_tasks()]
    seg_files = [task_count_files_and_objects(task_id) for task_id in seg]
    return seg_files



#================================================================================

def add_id(d):
    global tree_json_cnt
    d['id']=tree_json_cnt
    
    tree_json_cnt+=1
    for j in range(len(d['children'])):
        
        add_id(d['children'][j])



def root_tree_json(treejson):
    global tree_json_cnt
    tree_json_cnt=0
#     os.system("tree -J -d -o foldertree.json "+root_directory) 
    with open(treejson) as f:
        data = str(json.load(f))     
    res=data.replace("'type': 'directory', ","").replace("contents","children").replace("name","label")
    temp1=eval(res[:res.find(", {'type': 'report', 'directories'")]+']')
    add_id(temp1[0])
    return temp1

@tasksBP.route('/folder_tree')
def folder_tree():
    '''API call - returns a list of folders under direc'''
      
    st=os.stat(FOLDER_TREE)    
    mtime=st.st_mtime
    print(time.time()-mtime)
    timesince=time.time()-mtime
    if timesince>300:
        print('scaning dir')
        try:
            res=jsonify(root_tree_json(FOLDER_TREE) )
            cmd_str=f"tree -J -o {FOLDER_TREE} {ROOT_DIR} && tree -J -o {FOLDER_TREE_2} {ROOT_DIR}"
            proc = subprocess.Popen([cmd_str], shell=True, stdin=None, stdout=None, stderr=None, close_fds=True)  
           
        except:
            pass
    else:
        try:
            res=jsonify(root_tree_json(FOLDER_TREE_2) )
        except:
            res=jsonify(root_tree_json(FOLDER_TREE) )
         
    
    return res





@tasksBP.route('/get_child_directories')
def get_child_directories():
    global direc
    '''API call - returns a list of folders under direc'''
    return jsonify([os.path.join(direc, o) for o in os.listdir(direc) if os.path.isdir(os.path.join(direc,o))] )


@tasksBP.route('/user_list')
def user_list():
    '''API call - returns a list of folders under direc'''
    return jsonify(users_list)
#================================ API =======================================
@tasksBP.route('/get_tasks_by_user/<user_name>')
def get_tasks_by_user(user_name):
    '''API call - returns the tasks associated with a user'''
    return jsonify( [t for t in get_tasks()] )

@tasksBP.route('/add_task', methods=['POST'])
def add_new_task():
    '''API call - creating a new task'''
    return jsonify( add_task(  json.loads(request.form['json']) ) )


@tasksBP.route('/delete_task/<task_id>')
def delete_task(task_id):
    '''API call - returns the tasks associated with a user'''
    return jsonify( remove_task(task_id) )


@tasksBP.route('/get_task/<task_id>')
def get_task(task_id):
    '''API call - returns the task associated with a task_id'''
    return jsonify(get_task_by_id())

@tasksBP.route('/get_segmentation_tasks_stats')
def get_segmentation_tasks_stats_api():
    return jsonify(get_segmentation_tasks_stats())