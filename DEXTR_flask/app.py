from collections import OrderedDict
from PIL import Image
import numpy as np
import cv2
import torch
from torch.nn.functional import upsample
from torch.autograd import Variable
import helpers
import networks.deeplab_resnet as resnet
from flask import Flask, request
import json

def get_dextr_model(fname = 'models/dextr_pascal-sbd.pth'):
    net = resnet.resnet101(1, nInputChannels=4, classifier='psp')
    state_dict_checkpoint = torch.load(fname, map_location=lambda storage, loc: storage)
    net.load_state_dict(state_dict_checkpoint)
    net.eval()
    return net

try: net
except: net = get_dextr_model()

def generate_input_dextr(image_path, points, pad = 50):
    '''
        image_path is a path to a jpg image file.
        points is a list of 4 (x,y) lists ex. [(489, 317), (520, 315), (507, 328), (502, 299)]
    '''
    image = np.array(Image.open(image_path))
    
    extreme_points_ori = np.array(points, dtype=int)
    bbox = helpers.get_bbox(image, points=extreme_points_ori, pad=pad, zero_pad=True)
    crop_image = helpers.crop_from_bbox(image, bbox, zero_pad=True)
    resize_image = helpers.fixed_resize(crop_image, (512, 512)).astype(np.float32)
    
    extreme_points = extreme_points_ori - [np.min(extreme_points_ori[:, 0]), np.min(extreme_points_ori[:, 1])] + [pad, pad]
    extreme_points = (512 * extreme_points * [1 / crop_image.shape[1], 1 / crop_image.shape[0]]).astype(np.int)
    
    extreme_heatmap = helpers.make_gt(resize_image, extreme_points, sigma=10)
    extreme_heatmap = helpers.cstm_normalize(extreme_heatmap, 255)
    
    input_dextr = np.concatenate((resize_image, extreme_heatmap[:, :, np.newaxis]), axis=2)

    input_dextr = torch.from_numpy(input_dextr.transpose((2, 0, 1))[np.newaxis, ...])

    inputs = Variable(input_dextr, volatile=True)
    
    return inputs, bbox, image

def get_contour_using_dextr(image_path, points, pad = 50, thres = 0.8, net = net):
    inputs, bbox, image = generate_input_dextr(image_path, points, pad)
    
    outputs = net.forward( inputs )
    outputs = upsample(outputs, size=(512, 512), mode='bilinear')
    
    pred = np.transpose(outputs.data.numpy()[0, ...], (1, 2, 0))
    pred = 1 / (1 + np.exp(-pred))
    pred = np.squeeze(pred)
    
    result = helpers.crop2fullmask(pred, bbox, im_size=image.shape[:2], zero_pad=True, relax=pad) > thres  #mask
    contour = cv2.findContours(result.copy().astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2:][0][0]
    
    return np.squeeze(contour).T.tolist()

app = Flask(__name__)

@app.route("/", methods=['GET'])
def dextr_get():
    return "hello DEXTR"

@app.route("/", methods=['POST'])
def dextr_service():
    '''input example: {"image_path" : "ims/IMG_2764.JPG", "points" : [[489, 317], [520, 315], [507, 328], [502, 299]]}'''
    j = request.get_json(force=True)
    j = json.loads(j)
    cont = get_contour_using_dextr(j['image_path'],  j['points'])
    return str(cont)

if __name__ == '__main__':
    app.run(port=50122)