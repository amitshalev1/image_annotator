import numpy as np
import pandas as pd
import joblib
import os
import cv2
import hashlib
import time
import glob
from tqdm import tqdm
import imagehash
from PIL import Image
import bcolz
from keras.applications.mobilenet import preprocess_input
from keras.preprocessing.image import img_to_array, load_img
#class plan
#------------ utils ------------------------



def resize_image(path, target_path, size=224): 
    fname = path.split('/')[-1]
    fname = f'small_{size}_' + fname
    target_path = target_path + fname
    try:
        if not os.path.isfile(target_path):
            im = cv2.imread(path)
            h,w,c = im.shape
            if w < h : width = size; height = int(size/w * h)
            else:      height = size; width = int(size/h * w)
            im = cv2.resize(im, (width, height))

            cv2.imwrite(target_path, im)
        return target_path
    except:
        return 'None'

def hash_image(source, target_path, target_size=None):
    #https://github.com/JohannesBuchner/imagehash
    try:
        im = Image.open(source)
        hash_name = str(imagehash.average_hash(im))
        fname = (target_path + '/' + hash_name + '.jpg').replace('//', '/').replace('\\', '/')
        im.save(fname, "JPEG")
    except:
        hash_name = 'None'
    return hash_name
            

def map_directory(path):
    print('Mapping Directory', path)
    if not path.endswith('/') : path = path+'/'
    imgs = np.unique([f.replace('\\', '/') for f in tqdm(glob.iglob(path + '**/*.*', recursive=True)) if f[-3:] in ['jpg', 'JPG', 'png', 'PNG']])
    return pd.DataFrame(imgs, columns=['PATH'])


def img_as_arr(path):
    return preprocess_input(img_to_array(load_img(path, target_size=(224,224))))

#=======================================================                 
class ImageDataset(object):
                 
    def __init__(self, project_name, imgs_path='/', creator='unnamed', to_hash= False, to_minimize=False, to_bcolz=False):
        self.name = project_name
        self._load_project_data(project_name, imgs_path, creator)
        if not self.images_mapped:
            self._prepare_images(to_hash, to_minimize, to_bcolz)
        self._load_mapped_dataset()
            
    
    def _load_project_data(self, *kwrds): 
        if not os.path.isdir('cache'): os.mkdir('cache')
        
        if not os.path.isfile(f'cache/{self.name}.joblib'):
            self.project_name, self.imgs_path, self.creator = kwrds[:3]
            self.creation_date = time.strftime('%Y-%m-%d %H:%M:%S')
            self.images_minized = False
            self.images_converted = False
            self.images_proccessed_by_nn = False
            self.images_hashed = False
            self.images_mapped = False
            self._cache_project_data()
        else: 
            self.__dict__ = joblib.load(f'cache/{self.name}.joblib')
    
        
    def set_param(self, **kwrds):
        keys = self.__dict__.keys()
        for k, v in kwrds.items():
            assert k in keys, f'Error:{k} not in ImageDataset'
            assert type(self.__dict__[k]) == type(v), f'TypeError: wrong type for "{k}": expected {type(self.__dict__[k])}, got: {type(v)}'
            self.__dict__[k] = v
        self._cache_project_data()
            
    
    def _cache_project_data(self): 
        proj_data = {k:v for k,v in vars(self).items() if k not in['nn_last_layer','dataset']}
        joblib.dump(proj_data, f'cache/{self.name}.joblib', compress=5) 
    
    #----------------------------------------------------
    def _map_images(self, fname):
        if not os.path.isfile(fname):
            if os.path.isdir(self.imgs_path):       df = map_directory(self.imgs_path)
            elif self.imgs_path.endswith('.json'):  df = pd.read_json(self.imgs_path)
            elif self.imgs_path.endswith('.csv'):   df = pd.read_csv(self.imgs_path)
            elif self.imgs_path.endswith('.tab'):   df = pd.read_table(self.imgs_path)
            else:
                raise ValueError('path must be either a valid path, json, csv or tab file!')
            df.to_parquet(fname)
            self.images_mapped = True
            self._cache_project_data()
            self.dataset = df
    
    def _clear_problems(self):
        fname = f'cache/{self.name}_dataset_map.parquet'
        self.dataset = self.dataset = self.dataset[self.dataset['HASH']!='None']
        self.dataset.to_parquet(fname)
        return self.dataset
    
    def _hash_images(self, fname):
        target_path = f'cache/project_hashed_images{self.name}'
        if not os.path.isdir(target_path): os.makedirs(target_path)
        hashed = [hash_image(path, target_path) for path in tqdm(self.dataset['PATH'].values)]
        self.dataset['HASH'] = hashed
        self.dataset = self.dataset[self.dataset['HASH'] != 'None']
        self.dataset.to_parquet(fname)
        self.images_hashed = True
        self._cache_project_data()
      
    
    def _reduce_images_size(self, fname, size):
        if not self.images_hashed: self._hash_images(fname)
        imgs = [f'cache/project_hashed_images{self.name}/' + f +'.jpg' for f in self.dataset['HASH'].values]
        target_path = f'cache/project_small_{self.name}/'
        if not os.path.isdir(target_path): os.makedirs(target_path)
        small = [resize_image(path, target_path, size) for path in tqdm(imgs)]
        
        self.dataset['SMALL'] = small
        self.dataset.to_parquet(fname)
        self.images_minized = True
        self._cache_project_data()
        
        
    def _images_to_bcolz(self, fname, size):
        print('creating bcolz')
        if not self.images_minized: self._reduce_images_size(fname, size)
        bcolz_dir = f'cache/project_bcolz_{self.name}/'
        if not os.path.isdir(bcolz_dir):
            first_item = img_as_arr(self.dataset['SMALL'].values[0])
            first_item = np.expand_dims(first_item, axis=0)
            b = bcolz.carray(first_item, rootdir=bcolz_dir, mode='w')
            
            for path in tqdm(self.dataset['SMALL'].values[1:]):
                im = img_as_arr(path)
                b.append(im)
                b.flush()
        self.images_converted = True
        self._cache_project_data()
        
    #----------------------------------------------------
    def _prepare_images(self, to_hash, to_minimize, to_bcolz, size=224):
        
        fname = f'cache/{self.name}_dataset_map.parquet'
        
        if not self.images_mapped:
            self._map_images(fname)
        
        if to_hash and self.images_hashed == False:
            self._hash_images(fname)
            
        if to_minimize and self.images_minized == False:
            self._reduce_images_size(fname, size)
            
        if to_bcolz and self.images_converted == False:
            self._images_to_bcolz(fname, size)
            
    #----------------------------------------------------
    def _pass_trough_nn(self, net='resnet50'):
        if self.images_converted == False:
            self._prepare_images(True, True, True)
        
        if self.images_proccessed_by_nn == False:
            if not os.path.isfile(f'cache/{net}_last_layer_{self.name}.parquet'):
                x = bcolz.open(rootdir=f'cache/project_bcolz_{self.name}')
                if net == 'resnet50':
                    from keras.applications.resnet50 import ResNet50
                    nn = ResNet50(include_top=False, weights='imagenet', input_shape=(224,224,3), pooling='avg')
                nn.compile('sgd','categorical_crossentropy')
                self.nn_last_layer = nn.predict(x, verbose=True)
                
                self.nn_last_layer = pd.DataFrame(self.nn_last_layer)
                self.nn_last_layer.columns = ['rn50_feature_%s'%(n) for n in self.nn_last_layer.columns]
                self.nn_last_layer.to_parquet(f'cache/{net}_last_layer_{self.name}.parquet')
                self.images_proccessed_by_nn == True
                self._cache_project_data()
                return
        
        self.nn_last_layer = pd.read_parquet(f'cache/{net}_last_layer_{self.name}.parquet')
        
            
    def _pca(self, n_components = None):
        
        if not hasattr(self, 'nn_last_layer'): self._pass_trough_nn()
        fname = f'cache/pca_{n_components}_last_layer_{self.name}.parquet'
        if not hasattr(self, 'pca'):
            try:
                self.pca = pd.read_parquet(fname)
            except:
                print ('Running PCA...')
                svd_solver = 'auto'
                if n_components == None: 
                    n_components == 'mle'; 
                    svd_solver = 'full'
                from sklearn.decomposition import PCA
                pca = PCA(n_components = n_components, svd_solver=svd_solver)
                self.pca = pca.fit_transform(self.nn_last_layer.values)
                self.pca = pd.DataFrame(self.pca, columns = [f'pca_feature_{i}' for i in range(self.pca.shape[1])])
                self.pca.to_parquet(fname)
        
        return self.pca
    
    
    def _tsne(self, n_components = 2):
        
        if not hasattr(self, 'nn_last_layer'): self._pass_trough_nn()
        fname = f'cache/tsne_{n_components}_last_layer_{self.name}.parquet'
        if not hasattr(self, 'tsne'):
            try:
                self.tsne = pd.read_parquet(fname)
            except:
                print ('Running TSNE...')
                from MulticoreTSNE import MulticoreTSNE as TSNE
                tsne = TSNE(n_components = n_components, n_jobs=4)
                self.tsne = tsne.fit_transform(self.nn_last_layer.values)
                self.tsne = pd.DataFrame(self.tsne, columns = [f'tsne_feature_{i}' for i in range(self.tsne.shape[1])])
                self.tsne.to_parquet(fname)
        
        return self.tsne
            
            
            
            
            
    def _load_mapped_dataset(self):
        fname = f'cache/{self.name}_dataset_map.parquet'
        self.dataset = pd.read_parquet(fname)
        return self.dataset
    
    
    def cluster(self, on='pca', how='kmeans', n_clusters=20):
        '''performs clustering'''
        fname = f'cache/{self.name}_dataset_cluster_{how}_{n_clusters}.joblib'
        try:
            self.clusters = joblib.load(fname)
        except:
            #on ------------
            if on == 'pca':
                if not hasattr(self, 'pca'): self._pca()
                x = self.pca.values
            
            if on == 'tsne':
                if not hasattr(self, 'tsne'): self._tsne()
                x = self.tsne.values
            else:
                if not hasattr(self, 'nn_last_layer'): self._pass_trough_nn()
                x = self.nn_last_layer.values
            
            #how ------------
            if how=='kmeans' : 
                from sklearn.cluster import KMeans as KM
                if len(x) > 10000:  from sklearn.cluster import MiniBatchKMeans as KM
                clusterer = KM(n_clusters=n_clusters)
            
            if how=='meanshift' :
                from sklearn.cluster import MeanShift as MS
                clusterer = MS(min_bin_freq = 5, n_jobs = -2)
            
            print('Clustering...')
            clusterer.fit(x)
            self.clusters = clusterer.labels_
            joblib.dump(self.clusters, fname)
            
        self.dataset[f'cluster_{on}_{how}_{n_clusters}'] = self.clusters
        ds_fname = f'cache/{self.name}_dataset_map.parquet'
        self.dataset.to_parquet(ds_fname)
            
        return self.clusters
                    
        
    
    def __repr__(self):
        return f'''"{self.name}" <ImageDataset Class> 
       creator: {self.creator}
       path:"{self.imgs_path}"
       created:{self.creation_date}
       {len(self.dataset)} images
       '''
    
    @property
    def json(self, fname=None): pass
    
    
    def to_csv(self, fname=None): pass
    
    #------------  Image fetching --------------------------
    def pick_images_by_clusters(self, what = 'SMALL', on='pca', how='kmeans', n_clusters=20, return_coordinates='tsne', sample=False):
        
        self.cluster(on,  how, n_clusters)
        if return_coordinates == 'tsne':
            coor = self._tsne()
        df = pd.concat([self.dataset[[what, f'cluster_{on}_{how}_{n_clusters}']], coor], axis=1)
        df.columns = ['img', 'cluster', 'x', 'y']
        return df

    
    def group_images_by_clusters(self, what = 'SMALL', on='pca', how='kmeans', n_clusters=20, return_coordinates='tsne', sample=False):
        '''group images by clustering technique and returns a dict of lists
           with structure: {cluster_num : [{img: ..., x: ...,  y ...}] ...}
        '''
        df = self.pick_images_by_clusters(what, on, how, n_clusters, return_coordinates, sample)
        return {clust : imgs[['img', 'x', 'y']].to_dict(orient='records') 
                  for clust, imgs in df.groupby('cluster')}
        
        
    def visualize_images_by_clusters(self, what = 'SMALL', on='pca', how='kmeans', n_clusters=20, return_coordinates='tsne', sample=False, show_max = None, zoom=0.1, figsize=[10,10]):
        '''matplotlib utility to visuzlize clusters'''
        
        df = self.pick_images_by_clusters(what, on, how, n_clusters, return_coordinates, sample)
        if show_max == None:
            df_uniques = df.drop_duplicates('cluster')
        else:
            df_uniques = pd.concat([df[df['cluster'] == c].sample(show_max, replace = True) 
                             for c in df['cluster'].unique()])
            
            df_uniques = df_uniques.drop_duplicates()
            
        import matplotlib.pyplot as plt
        from matplotlib.offsetbox import OffsetImage, AnnotationBbox
        plt.figure(figsize=figsize)
        ax = plt.gca()
        ax.scatter(*df[['x', 'y']].T.values, c=df['cluster'])
        #plot the images...
        
        for img, x, y in tqdm(df_uniques[['img', 'x', 'y']].values):
            im = plt.imread(img)
            im = OffsetImage(im, zoom=zoom)
            ab = AnnotationBbox(im, (x, y), xycoords='data', frameon=False)
            ax.add_artist(ab)
        return ax