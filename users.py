from flask import Blueprint, request, jsonify, session
import json
from phenomics import auth

USERS_FILE = 'data/users.json'

usersBP = Blueprint('usersBP', __name__)
IS_ENV_PHENOMICS=True

#--------------------------- Helper functions --------------------------------
def save_users(users):
    '''function to push users into the USERS_FILE'''
    with open(USERS_FILE, 'w') as f:
            f.write(json.dumps(users))

def get_users():
    '''an helper function to load all users from USERS_FILE...'''
    try:
        with open(USERS_FILE) as f:
            return json.load(f)
    except:
        users = []
        save_users(users)
        return []

def get_next_user_id(users):
    '''helper function to return the max user_id+1'''
    ids = [int(u['user_id']) for u in users]
    if len(ids) == 0:
        return 0
    else:
        return max(ids)

def hash_password(password):
    assert len(password) > 0, 'No password provided!'
    import hashlib
    return hashlib.sha1(password.encode('utf-8')).hexdigest()

def user_exist(users, name):
    '''iterate trough users and check if the name exists'''
    if len([u for u in users if u['name'] == name])>0:
        return True
    return False 

def add_user(name, password):
    '''Adds a user and hashed password to the users database'''
    users = get_users()
    if not user_exist(users, name):
        users.append({'user_id' : get_next_user_id(users), 
                      'password': hash_password(password),
                      'name' : name,
                      'is_admin' : 0})
        save_users(users)
    return users

    

def login_check(name, password, users):
    password = hash_password(password)
    if password == [u for u in users if u['name'] == name][0]['password']:
        return 'login ok'
    else:
        return 'wrong password'


def user_register_or_login(name, password):
    if IS_ENV_PHENOMICS:
        return auth(name,password)
    else:
        users = get_users()
        if user_exist(users, name):
            return login_check(name, password, users)
        else:
            add_user(name, password)
            return '%s registered'%(name)

#============================================================================
#==================        Users API               ==========================
#============================================================================
        

@usersBP.route('/login_or_register', methods=['POST'])
def login_or_register():
#     user_phenp.login(request.form['name'],request.form['password'])
    name = request.form['name']
    password = request.form['password']
    results = user_register_or_login(name, password)
    if not results == 'wrong password':
        session['current_user'] = name
        print (session['current_user'] )
    return 'logged in succefully'

@usersBP.route('/check_if_loginned', methods=['GET'])
def check_if_loginned():
    '''This API call will check if the current user is already logged in'''
    if 'current_user' in session:
        return session['current_user']
    else:
        return 'not logged in'

@usersBP.route('/user_logout', methods=['GET'])    
def user_logout():
    if 'current_user' in session:
        del session['current_user']
    return "logout"
    

@usersBP.route('/is_admin/<name>')
def is_user_admin(name):
    return jsonify('1')


@usersBP.route('/make_admin/<name>')
def make_admin(name):
    users = get_users()
    if user_exist(users, name):
        user = [u for u in users if u['name'] == name][0]
        users = [u for u in users if u['name'] != name]
        user['is_admin'] = 1
        users.append(user)
        save_users(users)
        return jsonify(user)
    return 'user does not exists'

    