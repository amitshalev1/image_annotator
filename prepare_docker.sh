mkdir data
cd data/
echo "[]" >> datasets.json
echo '[{"name": "2017_01_22_cucumber_Blossom end shape", "img_source": "example/cucumbers/", "possible_values": "0,1,2,3,4,5,6,7,8,9", "trait": "Blossom end shape", "users": "Adam Call,Alain Lecompte,Harel Belotserkovsky", "img_target": "772", "type": "simple scoring", "crop": "cucumber", "object": "fruit", "dev": "mature", "treatment": "none", "anno_order": "random", "allow_repeat": "No", "user_name": "dror", "task_id": 2}]' >> tasks.json

echo '[{"user_id": 0, "password": "3558dd008650017d9c6f23ec9da644a85eeb3664", "name": "dror", "is_admin": 1}, {"user_id": 0, "password": "7288edd0fc3ffcbe93a0cf06e3568e28521687bc", "name": "test", "is_admin": 0}, {"user_id": 0, "password": "3d0f3b9ddcacec30c4008c5e030e6c13a478cb4f", "name": "daniel", "is_admin": 1}]
' >> users.json
echo "[]" >> dataset_creation_log.json
mkdir results
cd ..
cd img/
cp lens-833059_640_bdark.jpg lens-833059_640_dark.jpg
cd ..

python -m flask run -h 0.0.0.0 -p 5002


